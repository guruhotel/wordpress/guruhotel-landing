<?php
/* Template name: Landings Page Healthcare Workers */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section id="healthcare--landing">
        <div class="healthcare--landing__banner">
            <div class="healthcare--landing__banner--bg" style="background-image: url('<?php echo assets_url(); ?>/images/healthcare-bg.png');"></div>
            <div class="container-fluid wrap" style="position: relative; z-index: 444;">
                <div class="row center-xs start-md">
                    <div class="col-xs-12 col-md-6 center-xs start-md">
                        <div class="banner-text">
                            <h1 class="banner--title">Ayudamos a quienes cuidan de los nuestros.</h1>
                            <p class="banner--desc">En agradecimiento a los servicios que prestas en el día a día en pro de las personas que te rodean, <strong>GuruHotel quiere ofrecerte un descuento especial en nuestros hoteles equivalente al 10% de tu reserva</strong>.</p>
                            <div class="banner--promo">
                                <span class="label">Código del cupón:</span>
                                <span class="coupon">GURUCARE10</span>
                            </div>
                        </div>
                        <div class="banner--features">
                            <div class="row start-xs">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="feature--card">
                                        <i class="fas fa-heartbeat"></i>
                                        <h2 class="title">Profesionales de la salud</h2>
                                        <p class="desc">Actividades relacionadas al cuidado de la salud de las personas y mascotas.</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="feature--card">
                                        <i class="fas fa-shield-alt"></i>
                                        <h2 class="title">Servidores públicos</h2>
                                        <p class="desc">Oficiales de seguridad, bomberos y servicios de emergencia.</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="feature--card">
                                        <i class="far fa-id-card"></i>
                                        <h2 class="title">Primera necesidad</h2>
                                        <p class="desc">Servicios de transporte de mercancías y telecomunicaciones.</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="feature--card">
                                        <i class="far fa-life-ring"></i>
                                        <h2 class="title">S.O.S extranjeros</h2>
                                        <p class="desc">Turistas con problemas para regresar a su país de origen a causa de la pandemia.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section id="partners">
            <div class="container-fluid wrap">
                <div class="row center-xs">
                    <div class="col-xs-12 col-md-5">
                        <p class="title text-color__text line-height__big--x font-weight__bold">Algunos de nuestros hoteles:</p>
                    </div>
                </div>
                <div class="row center-xs middle-xs">
                    <div class="col-xs-12 logos">
                        <div class="logo">
                            <a href="https://www.maglenresort.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/maglen.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://www.puertoaventurashotel.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/puertoaventuras.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://portobelloaparthotel.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/granmarina.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://elementustulum.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/elementus.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://hotelcaribbeanparadise.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/caribbeanparadise.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://lacasadelalux.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/lacasadelalux.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://fincacoyoacan.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/fincacoyoacan.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://vlandrebacalar.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/vlandre.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://www.oceandreamcancun.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/oceandream.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://distritocorazonplaya.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/distritocorazon.svg"></a>
                        </div>
                        <div class="logo">
                            <a href="https://ahavaplaya.com/" target="_blank" rel="nofollow"><img src="<?php assets_url(); ?>/images/hotels/ahava.svg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="healthcare--landing__footer">
            <div class="container-fluid wrap">
                <div class="row">
                    <div class="col-xs-12">
                        <img src="<?php assets_url(); ?>/images/logo-original.svg" class="footer-logo">
                        <p>&copy; <?php echo date('Y'); ?> <strong class="titles-color">GuruHotel</strong> - Cadena Hotelera Digital</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script>
new WOW().init();
</script>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();