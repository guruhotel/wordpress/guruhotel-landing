<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header id="header">
	<div class="container-fluid wrap">
		<div class="row middle-xs">
			<div class="logo col-xs-5 col-md-3">
				<?php if(is_front_page() || is_page_template('pricing.php') || is_page_template('landing-healthcare.php')) { ?>
					<a href="<?php bloginfo('wpurl'); ?>">
						<img src="<?php assets_url(); ?>/images/logo-original.svg">
					</a>
				<?php } elseif (is_page_template('saving.php') || is_page_template('savingbuy.php')) { ?>
					<a href="<?php bloginfo('wpurl'); ?>/saveindiehotels">
						<img src="<?php assets_url(); ?>/images/saveindiehotels-logo.svg">
					</a>
				<?php } else { ?>
					<a href="<?php bloginfo('wpurl'); ?>">
						<img src="<?php assets_url(); ?>/images/logo-inverse.svg">
					</a>
				<?php } ?>
				<button class="navicon">
					<img src="<?php assets_url(); ?>/images/navicon.svg">
				</button>
			</div>
			<div class="menu col-xs-7 col-md-9 end-xs">
				<nav class="main-menu">
					<ul>
						<li class="lang">
							<?php echo do_shortcode('[language-switcher]'); ?>
						</li>
						<li class="pms">
							<a href="<?php bloginfo( 'wpurl' ); ?>/login"><i class="far fa-user-circle text-color__utilitary"></i> <span class="label">Login</span></a>
						</li>
						<?php if(is_page_template('saving.php')) { ?>
							<li class="redeem">
								<a class="btn background-color__white" href="https://app.guruhotel.com/redeem" target="_blank">Redimir</a>
							</li>
						<?php } ?>
						<li class="active">
							<a href="<?php bloginfo('wpurl'); ?>/planes">Planes</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>

<main id="main">