<?php
/* Template name: Case studies: Single */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="page-template-landing">
	<div class="titles-bg hero-page hero-two" <?php if( has_post_thumbnail() ) { echo 'style="background-image: url('.get_the_post_thumbnail_url().')"'; } ?>>
		<div class="container-fluid wrap">
			<div class="row middle-xs center-xs">
				<div class="col-xs-12 col-sm-6 col-md-7">
					<div class="hero-caption center-xs">
						<?php
						$logo = get_post_meta(get_the_ID(), 'case_study_logo', true);
						if( !empty($logo) ) { ?>
							<img class="logo" src="<?php echo $logo; ?>" alt="<?php the_title(); ?>">
						<?php } ?>
						<hr>
						<?php
						$subline = get_post_meta(get_the_ID(), 'landing_subline', true);
						if( !empty($subline) ) { ?>
							<p class="text-color__white font-size-medium wow fadeInUp margin-top__small margin-bottom__small" data-wow-delay="0.2s"><?php echo $subline; ?></p>
						<?php } ?>
						<h1 class="font-size-medium wow fadeInUp text-color__secondary margin-bottom__top">- <?php the_title(); ?></h1>
					</div>
				</div>
				<?php
				$main_features = get_post_meta(get_the_ID(), 'landing_main_features', true);
				if( !empty($main_features) ) { ?>
					<div class="col-xs-12">
						<hr class="alpha-bg">
						<div class="row center-xs">
							<?php
							foreach ( $main_features as $feature ) { ?>
								<div class="feature wow fadeInUp col-xs-12 col-sm-6 col-md-4 margin-bottom__medium">
									<i class="<?php echo $feature['icon']; ?> icon font-size-x-medium text-color__secondary"></i>
									<h3 class="title text-color__white font-size-medium margin-top-normal">
										<?php echo $feature['title']; ?>
									</h3>
									<p><?php echo $feature['desc']; ?></p>
								</div>
							<?php } ?>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php if( '' !== get_post()->post_content ) { ?>
		<div class="background-color__white padding-top__big--x padding-bottom__big--x">
			<div class="container-fluid wrap">
				<div class="row center-xs">
					<div class="col-xs-12 col-md-9 start-xs">
						<div class="the-content"><?php the_content(); ?></div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php
	$form_shortcode = get_post_meta(get_the_ID(), 'landing_form_shortcode', true);
	$form_content = get_post_meta(get_the_ID(), 'landing_form_content', true);
	if( $form_shortcode || $form_content ) { ?>
		<div id="contact-form" class="background-color__titles">
			<div class="container-fluid wrap">
				<div class="row center-xs">
					<div class="col-xs-12 col-md-8 start-xs">
						<div class="the-content white">
							<?php
							if( !empty($form_content) ) {
								echo wpautop($form_content);
							} ?>
						</div>
						<div id="page-form" class="page-form two">
							<?php
							if( $form_shortcode ) {
								echo do_shortcode($form_shortcode);
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php
	$cta_title = get_post_meta(get_the_ID(), 'landing_cta_title', true);
	$cta_text = get_post_meta(get_the_ID(), 'landing_cta_text', true);
	$cta_btn_text = get_post_meta(get_the_ID(), 'landing_cta_btn_text', true);
	$cta_btn_url = get_post_meta(get_the_ID(), 'landing_cta_btn_url', true);
	if($cta_title || $cta_text) { ?>
		<div class="page-cta background-color__titles padding-top__big--x padding-bottom__big--x">
			<div class="container-fluid wrap">
				<div class="row center-xs">
					<div class="col-xs-12 col-md-8 center-xs">
						<?php if( $cta_title ) { ?>
							<h2 class="line-height__big text-color__secondary"><?php echo $cta_title; ?></h2>
						<?php } ?>
						<?php if( $cta_text ) { ?>
							<p class="font-size__big text-color__white margin-bottom__big--x"><?php echo $cta_text; ?></p>
						<?php } ?>
						<?php if( $cta_btn_text ) { ?>
							<a href="<?php echo $cta_btn_url; ?>" class="btn wow fadeInUp background-color__secondary font-size__medium btn__size--medium text-color__titles" data-wow-delay="0.4s"><?php echo $cta_btn_text; ?></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
</section>

<script>
new WOW().init();
</script>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();