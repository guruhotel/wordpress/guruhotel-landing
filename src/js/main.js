var j = jQuery.noConflict();

j(document).ready(function () {
    j("[data-toggle='body']").click(function () {
        var e = j(this).data("toggle");
        j(e).toggleClass("open-menu");
    });

    j('.case-studies-slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        fade: false,
        arrows: true,
        variableWidth: true,
        pauseOnFocus: true,
        pauseOnHover: true,
        autoplay: true,
        autoplaySpeed: 4000,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    centerMode: true,
                    centerPadding: '0px'
                }
            }
        ]
    });

    j('.testimonials').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 6000,
        pauseOnFocus: true,
        pauseOnHover: true
    });
});

var mediaquery = window.matchMedia("(min-width: 768px)");
if (mediaquery.matches) {
    new WOW().init();
}