<?php
/* Template name: Case studies: Archive */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php
$case_studies = theme_opt('theme_studies','case_studies');
if( !empty( $case_studies ) ) { ?>
    <section id="case-studies" class="container-fluid wrap">
        <div class="row">
            <div class="col-xs-12 center-xs margin-top__big--x">
                <h1 class="font-size__big margin-bottom__small">Casos de éxito</h1>
                <p class="margin-top__small font-size__medium--x text-color__titles">Selección de historias reales</p>
                <div class="studies-row">
                    <?php foreach ( $case_studies as $case ) { ?>
                        <div class="content-study">
                            <div class="card card__size--small background-color__white margin-top__big border-radius__normal margin-bottom__big box-shadow__mega">
                                <div class="thumb thumb__bg--image" style="background-image: url('<?php echo $case['image']; ?>');">
                                    <img src="<?php echo $case['image']; ?>">
                                </div>
                                <div class="caption">
                                    <img class="logo" src="<?php echo $case['logo']; ?>" alt="CasonaDelSol">
                                    <p class="quote"><?php echo $case['cite']; ?></p>
                                    <a href="<?php echo $case['url']; ?>" class="btn btn__size--normal background-color__transparent border-color__white text-color__white">Leer historia completa</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();