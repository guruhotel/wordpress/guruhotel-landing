<?php 
get_header();  ?>
<section id="apollo__main-banner">
    <div class="container-fluid wrap">
        <div class="row middle-xs center-xs">
            <div class="col-xs-12">
                <div class="banner-text">
                    <p class="banner-preline wow fadeInUp" data-wow-delay="0.2s">Bienvenido a la hotelería del futuro</p>
                    <h1 class="banner-title wow fadeInUp" data-wow-delay="0.4s">Recupera el control de tu hotel <span class="text-color__utilitary">optimizando tu canal directo</span></h1>
                    <p class="banner-desc wow fadeInUp" data-wow-delay="0.6s">Todo lo que necesitas, desde un mismo centro de mando.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="banner-image center-xs">
        <img src="<?php assets_url(); ?>/images/website-guruhotel.svg" class="banner--image__website" alt="GuruHotel Hotels Website">
        <img src="<?php assets_url(); ?>/images/website-guruhotel__booking.svg" class="banner--image__app booking wow fadeInLeft" data-wow-delay="1.4s" alt="GuruHotel Hotels Apps Booking.com">
        <img src="<?php assets_url(); ?>/images/website-guruhotel__google.svg" class="banner--image__app google wow fadeInLeft" data-wow-delay="1.2s" alt="GuruHotel Hotels Apps Google">
        <img src="<?php assets_url(); ?>/images/website-guruhotel__pms.svg" class="banner--image__app pms wow fadeInLeft" data-wow-delay="1s" alt="GuruHotel Hotels Apps PMS">
        <img src="<?php assets_url(); ?>/images/website-guruhotel__checkout.svg" class="banner--image__checkout wow fadeInRight" data-wow-delay="1.5s" alt="GuruHotel Hotels Checkout">
    </div>
</section>

<section id="sections">
    <div class="container-fluid wrap wow fadeIn">
        <div class="row center-xs middle-xs">
            <div class="col-xs-10 col-sm-9 col-md-8 center-xs">
                <h2 class="sections--title line-height__big--x">GuruHotel es el primer <span class="text-color__utilitary">sistema operativo de hoteles (O.S)</span> basado en la nube.</h2>
            </div>
        </div>
        <div class="row margin-top__mega center-xs middle-xs">
            <div class="col-xs-12 col-sm-8 col-md-4 start-xs">
                <div class="card item one">
                    <i class="fas fa-shopping-cart icon"></i>
                    <h4 class="without-margin-top">Marketplace de apps</h4>
                    <p>Desde conexiones a OTA's (Ej. Booking.com), hasta herramientas de marketing, anuncios y tecnología.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-4 start-xs">
                <div class="card item two">
                    <i class="fas fa-globe icon"></i>
                    <h4 class="without-margin-top">Sitio web hotelero</h4>
                    <p>Tu propio sitio web ultra-optimizado para ventas autogenerado y administrado por ti.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-4 start-xs">
                <div class="card item three">
                    <i class="far fa-comments icon"></i>
                    <h4 class="without-margin-top">Comunidad de hoteleros</h4>
                    <p>Forma parte de nuestra cadena hotelera digital independiente y comparte con toda la comunidad.</p>
                </div>
            </div>
        </div>
    </div>

    <div id="apollo__covid">
        <div class="apollo__covid--cta">
            <div class="bg" style="background-image: url('<?php assets_url(); ?>/images/covid-guruhotel.jpg');"></div>
            <div class="container-fluid wrap">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h2 class="text-color__white">Después del covid-19 se demostró que hacerlo de forma tradicional no era suficiente.</h2>
                        <p class="text-color__white line-height__big--x">Necesitas poder tener el control de tus huéspedes en tiempo real dentro y fuera de tu hotel. Brindarles asistencia personalizada y una experiencia única que les brinde la sensación de querer regresar.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="apollo__howitworks">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-12 col-md-8 start-xs">
                    <p class="text-uppercase center-xs font-size__small--x">Hotelería inteligente</p>
                    <h2 class="center-xs">¿Cómo funciona?</h2>
                    <div class="howitworks--steps">
                        <div class="howitworks--step wow fadeInUp" data-wow-delay="0.2s">
                            <span class="number">1.</span>
                            <h3 class="label">Crea tu cuenta</h3>
                            <p class="desc">En 3 simples pasos donde nos brindas un poco de información básica acerca de tu hotel estarás dentro de tu dashboard.</p>
                        </div>
                        <div class="howitworks--step wow fadeInUp" data-wow-delay="0.4s">
                            <span class="number">2.</span>
                            <h3 class="label">Desbloquea tus conexiones</h3>
                            <p class="desc">Desde conexiones a OTA's como Booking.com, Expedia, etc. hasta herramientas de marketing como Google ads, Mailchimp, entre otras. Paga solo por lo que vas a usar.</p>
                        </div>
                        <div class="howitworks--step wow fadeInUp" data-wow-delay="0.6s">
                            <span class="number">3.</span>
                            <h3 class="label">Lanza tu sitio web</h3>
                            <p class="desc">Luego de completar los datos, conexiones e información de tu hotel, podrás lanzar tu sitio web por ti mismo con un solo click, y administrarlo desde tu dashboard.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="apollo__start--cta">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-12">
                    <div class="apollo__start--cta">
                        <img src="https://dev.guruhotel.com/images/illus/website-generator.svg" class="illus" />
                        <h2 class="margin-bottom__big">Probar GuruHotel es gratis.</h2>
                        <p>GuruHotel es la solución cost-effective para la hotelería.<br />Paga solamente por las conexiones o apps que actives.</p>
                        <a href="https://app.guruhotel.com/register?ref=guruhotel-2020" class="btn btn__size--big background-color__utilitary text-color__white ">Comenzar gratis</a>
                        <div class="row margin-top__mega">
                            <div class="col-xs-12">
                                <h5 class="margin-bottom__mega--x">¿Que viene incluido?</h5>
                            </div>
                            <div class="col-xs-12 col-sm-4 margin-bottom__mega">
                                <div class="card card__size--big start-xs starter--item border-radius__normal border-color__grey--regent box-shadow__small">
                                    <div class="text-color__titles margin-bottom__small--x font-size__medium"><i class="fas one fa-globe margin-right__normal"></i> Sitio web hotelero</div>
                                    <span class="display__block">(5% de comisión por reserva)</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 margin-bottom__mega">
                                <div class="card card__size--big start-xs starter--item border-radius__normal border-color__grey--regent box-shadow__small">
                                    <div class="text-color__titles margin-bottom__small--x font-size__medium"><i class="fas two fa-credit-card margin-right__normal"></i> Motor de reservas</div>
                                    <span class="display__block">Cobra 100% online</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 margin-bottom__mega">
                                <div class="card card__size--big start-xs starter--item border-radius__normal border-color__grey--regent box-shadow__small">
                                    <div class="text-color__titles margin-bottom__small--x font-size__medium"><i class="far three fa-comments margin-right__normal"></i> Cadena hotelera digital</div>
                                    <span class="display__block">Miembro de la comunidad</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
var j = jQuery.noConflict();

j(window).on('scroll', function() {
    if (isScrolledIntoView('#apollo__covid')) {
        j("body").addClass('darkMode');
    } else {
        j("body").removeClass('darkMode');
    }
});

function isScrolledIntoView(elem) {
    var docViewTop = j(window).scrollTop();
    var docViewBottom = docViewTop + j(window).height() * 0.5;
    var elemTop = j(elem).offset().top;

    return (elemTop <= docViewBottom);
}
</script>

<?php 
get_footer(); ?>