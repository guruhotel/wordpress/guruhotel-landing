<?php
/* Template name: Webinar */
get_header(); ?>

<section id="webinar">
    <div class="headbar">
        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-12 center-xs">
                    <div class="logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/webinar.svg" alt="GuruHotel Webinars">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="webinar-hero">
        <div class="container-fluid wrap">
            <div class="row center-xs middle-xs">
                <div class="col-xs-12 col-md-10">
                    <p class="webinar-hero-desc wow fadeInUp">Nuestro webinar #1 ha finalizado exitosamente.</p>
                    <h1 class="webinar-hero-title wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">El poder de la tecnología en la industria hotelera.</h1>
                    <p class="webinar-hero-desc wow fadeInUp" data-wow-delay="0.4s">Gracias a quienes participaron y a nuestros panelistas.</p>
                    <div>
                        <a class="btn btn__size--medium margin-top__medium font-size__medium border-color__secondary text-color__secondary" href="https://zoom.us/recording/share/1prdyWc6bjN1a60DhP4pMRNmTwcxmkgCHrt9CcVF9uCwIumekTziMw?startTime=1575910795000" rel="nofollow" target="_blank">
                            <i class="fas fa-play-circle"></i> Ver repetición
                        </a>
                    </div>
                    <img class="arrow" style="opacity: 0;" src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-down.svg">
                </div>
            </div>
        </div>
        <div class="webinar-hero-bg" style="background-image: url('https://guruhotel.mx/wp-content/uploads/2019/12/GuruHotel-Webinar.jpeg');"></div>
    </div>
    <div class="webinar-cards">
        <div class="container-fluid wrap">
            <div class="row around-xs middle-xs">
                <div class="col-xs-12 center-xs start-xs">
                    <div class="item start-xs subscribe card center-xs card__size--big border-radius__big">
                        <h2 class="font-size__medium--x text-color__white margin-top__normal margin-bottom__small line-height__medium--x">¿Deseas estar al tanto de nuestros próximos webinar?</h2>
                        <p class="text-color__main margin-top__big">Déjanos tus datos y se el primero en enterarte.</p>
                        <div id="page-form"><?php echo do_shortcode('[wpforms id="78"]'); ?></div>
                    </div>
                    <div class="item start-xs wow fadeInUp card card__size--big border-radius__big">
                        <img src="https://guruhotel.mx/wp-content/uploads/2019/11/Carla.png" class="avatar avatar__size--big border-radius__rounded" alt="Carla Marchena">
                        <h2 class="font-size__medium--x text-color__white margin-top__normal margin-bottom__small">Carla Marchena <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/carlamarchena/" class="text-color__whiter margin-left__normal"><i class="fab fa-linkedin text-color__secondary"></i></a></h2>
                        <p class="font-size__small--x text-color__main without-margin">Business Development Manager LATAM en OTA Insight</p>
                        <hr>
                        <h4 class="font-size__small text-transform__uppercase letter-spacing__normal text-color__secondary">Habló de:</h4>
                        <h3 class="text-color__white line-height__big">¿Qué son las disparidades y cómo te afectan?</h3>
                    </div>
                    <div class="item start-xs wow fadeInUp card card__size--big border-radius__big">
                        <img src="https://guruhotel.mx/wp-content/uploads/2019/11/jason.png" class="avatar avatar__size--big border-radius__rounded" alt="Jason lugo">
                        <h2 class="font-size__medium--x text-color__white margin-top__normal margin-bottom__small">Jason Lugo <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/jason-lugo/" class="text-color__whiter margin-left__normal"><i class="fab fa-linkedin text-color__secondary"></i></a></h2>
                        <p class="font-size__small--x text-color__main without-margin">Sr. Regional Sales Manager en SiteMinder</p>
                        <hr>
                        <h4 class="font-size__small text-transform__uppercase letter-spacing__normal text-color__secondary">Habló de:</h4>
                        <h3 class="text-color__white line-height__big">La distribución y los beneficios de estar conectado.</h3>
                    </div>
                    <div class="item start-xs wow fadeInUp card card__size--big border-radius__big">
                        <img src="https://guruhotel.mx/wp-content/uploads/2019/11/Leopoldo.png" class="avatar avatar__size--big border-radius__rounded" alt="Leopoldo Pérez">
                        <h2 class="font-size__medium--x text-color__white margin-top__normal margin-bottom__small">Leopoldo Pérez <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/leopoldo-p%C3%A9rez-728b7430/" class="text-color__whiter margin-left__normal"><i class="fab fa-linkedin text-color__secondary"></i></a></h2>
                        <p class="font-size__small--x text-color__main without-margin">Area Manager for South Mexico & Caribbean en Booking</p>
                        <hr>
                        <h4 class="font-size__small text-transform__uppercase letter-spacing__normal text-color__secondary">Habló de:</h4>
                        <h3 class="text-color__white line-height__big">El huésped como el centro de todo.</h3>
                    </div>
                    <div class="item start-xs wow fadeInUp card card__size--big border-radius__big">
                        <img src="https://guruhotel.mx/wp-content/uploads/2019/11/Josue.png" class="avatar avatar__size--big border-radius__rounded" alt="Josue Gio">
                        <h2 class="font-size__medium--x text-color__white margin-top__normal margin-bottom__small">Josue Gio <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/josuegio/" class="text-color__whiter margin-left__normal"><i class="fab fa-linkedin text-color__secondary"></i></a></h2>
                        <p class="font-size__small--x text-color__main without-margin">COO en GuruHotel</p>
                        <hr>
                        <h4 class="font-size__small text-transform__uppercase letter-spacing__normal text-color__secondary">Habló de:</h4>
                        <h3 class="text-color__white line-height__big">¿Qué hay más allá de un sitio web bonito?</h3>
                    </div>
                    <div class="item start-xs mod wow fadeInUp card card__size--big border-radius__big">
                        <img src="https://guruhotel.mx/wp-content/uploads/2019/12/JorgeFlores.png" class="avatar avatar__size--big border-radius__rounded" alt="Josue Gio">
                        <h2 class="font-size__medium--x text-color__white margin-top__normal margin-bottom__small">Jorge Flores <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/jorgefloresacevedo/" class="text-color__whiter margin-left__normal"><i class="fab fa-linkedin text-color__secondary"></i></a></h2>
                        <p class="font-size__small--x text-color__main without-margin">CEO en GuruHotel - <span class="font-size__small text-transform__uppercase letter-spacing__normal text-color__secondary">Moderador</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="webinar-affiliates backgroun-color__white">
        <div class="container-fluid wrap">
            <div class="row center-xs middle-xs">
                <div class="col-xs-12">
                    <h4 class="font-size__small text-transform__uppercase letter-spacing__normal text-color font-size__medium">Invitaron:</h4>
                    <img class="logo ota" src="https://guruhotel.mx/wp-content/uploads/2019/12/OTA-Insight_Horizontal-Logo_RGB_LightBackground-1024x225.png" alt="Ota Insight">
                    <img class="logo" src="https://guruhotel.mx/wp-content/uploads/2019/11/Booking.png" alt="booking">
                    <img class="logo" src="https://guruhotel.mx/wp-content/uploads/2019/11/SiteMinder.png" alt="SiteMinder">
                    <img class="logo" src="https://guruhotel.mx/wp-content/uploads/2019/11/guruhotel.png" alt="GuruHotel">
                </div>
            </div>
        </div>
    </div>
</section>

<script>
new WOW().init();
</script>

<script type="text/javascript">
    adroll_adv_id = "FXSJ2QSF45EWDPCORSNWY4";
    adroll_pix_id = "S23AFGNDJFE5XI3WJQ5NMI";

    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>

<?php 
get_footer();