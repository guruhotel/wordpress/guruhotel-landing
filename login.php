<?php
/* Template name: Login */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="login">
    <div class="container-fluid wrap">
        <div class="row center-xs middle-xs">
            <div class="col-xs-12 col-md-6 center-xs">
                <a href="<?php bloginfo('wpurl'); ?>">
                    <img class="logo" src="<?php assets_url(); ?>/images/guruhotelPMS.svg">
                </a>
                <div class="card start-xs border-radius__small--x box-shadow__medium border-color__grey--regent card__size--big">
                    <form role='form' action="https://admin.guruhotel.com/pms/hotels/login/access/" method="post">
                        <div class="margin-top__medium">
                            <label for="username">
                                <span>Usuario</span>
                                <input type="text" class="input text-align__left input__size--big input__width--full border-color__grey--regent box-shadow__normal border-radius__small" name="username" id="username" placeholder="Usuario">
                            </label>
                        </div>
                        <div class="margin-top__medium">
                            <label for="password">
                                <span>Contraseña</span>
                                <input type="password" class="input input__size--big input__width--full border-color__grey--regent box-shadow__normal border-radius__small" name="password" id="password" placeholder="Contraseña">
                            </label>
                        </div>
                        <button type="submit" class="btn margin-top__big btn__size--big background-color__utilitary text-color__white btn__size--full">Conectarse</button>
                        <div class="center-xs margin-top__big">
                            <span>¿No tienes cuenta aún?</span> <a href="<?php bloginfo('wpurl'); ?>/planes" class="highlight text-color__utilitary utilitary-border">Únete a GuruHotel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();