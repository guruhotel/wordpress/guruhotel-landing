'use strict';

module.exports = function(grunt) {
    // Unified Files
    var files = {
        mainJS: [
            'assets/lib/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
            'assets/lib/wow/dist/wow.js',
            'assets/lib/slick-carousel/slick/slick.js',
            'assets/js/main.js'
        ],
        defaultCSS: [
            'assets/css/reset.css',
            'assets/css/grid.css',
            'assets/lib/slick-carousel/slick/slick.css',
            'assets/lib/animate.css/animate.min.css'
        ],
        mainSCSS: [ 'assets/sass/main.scss' ]
    };

    // Start tasks
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // CSSMin and JS Uglify
        uglify: {
            production: {
                options: {
                    mangle: false
                },
                files: {
                    'assets/dist/main.min.js': files.mainJS,
                    'assets/dist/scrollmagic.min.js': files.scrollmagicJS,
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'assets/dist/default.min.css': files.defaultCSS,
                },
            }
        },

        //Watch files for Livereload
        watch: {
            sass: {
                files: ['assets/sass/*.scss'],
                tasks: ['sass'],
            },
        },

        // SASS Compiler
        sass: {                                  
            dist: {                                  
                options: {                         
                    style: 'compact'
                },
                files: {
                    'assets/dist/main.min.css': files.mainSCSS
                },
            }
        },

        // inside Gruntfile.js
        // Using the BrowserSync Proxy for your existing website url.
        browserSync: {
            default_options: {
                bsFiles: {
                    src: [
                    "assets/dist/*.css",
                    "*.html",
                    "*.php",
                    ]
                },
                options: {
                    watchTask: true,
                    proxy: "guruhotel.local"
                }
            }
        }
    });

    // Load NPM tasks
    require('load-grunt-tasks')(grunt);

    // Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    // Default task(s).
    grunt.registerTask('default', ['build']);

    // Build task(s).
    grunt.registerTask('build', ['sass','uglify','cssmin','browserSync','watch']);
};