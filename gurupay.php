<?php
/* Template name: GuruPay */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="page-template-landing">
	<div class="titles-bg hero-page hero-two">
		<div class="container-fluid wrap">
			<div class="row middle-xs center-xs">
				<div class="col-xs-12 col-sm-10 col-md-8">
					<div class="hero-caption">
                        <img src="https://guruhotel.mx/wp-content/uploads/2019/11/paybyguru.svg" style="max-width: 140px; margin-bottom: 20px;">
						<h1 class="font-size-x-big wow fadeInUp white-color without-margin-top"><?php the_title(); ?></h1>
                        <?php
						$subline = get_post_meta(get_the_ID(), 'landing_subline', true);
						if( !empty($subline) ) { ?>
							<p class="text-color__white font-size-medium wow fadeInUp margin-top-medium" data-wow-delay="0.2s"><?php echo $subline; ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php
	$main_features = get_post_meta(get_the_ID(), 'landing_main_features', true);
	if( !empty($main_features) ) { ?>
		<div class="container-fluid wrap template-landing-main-features margin-top-big margin-bottom-x-big">
			<div class="row center-xs">
				<?php
				foreach ( $main_features as $feature ) { ?>
					<div class="feature col-xs-12 col-sm-8 col-md-4">
						<div class="card border-color__grey--regent box-shadow__medium start-xs">
							<i class="<?php echo $feature['icon']; ?> icon font-size-x-medium main-color"></i>
							<h3 class="font-size-medium margin-top-normal">
								<?php echo $feature['title']; ?>
							</h3>
							<p class="text-color__titles"><?php echo $feature['desc']; ?></p>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
	<div class="container-fluid wrap">
		<div class="row center-xs">
			<div class="col-xs-12 start-xs">
				<div class="the-content"><?php the_content(); ?></div>
			</div>
		</div>
	</div>
    <?php
	$cta_title = get_post_meta(get_the_ID(), 'landing_cta_title', true);
	$cta_text = get_post_meta(get_the_ID(), 'landing_cta_text', true);
	$cta_btn_text = get_post_meta(get_the_ID(), 'landing_cta_btn_text', true);
	$cta_btn_url = get_post_meta(get_the_ID(), 'landing_cta_btn_url', true);
	if($cta_title || $cta_text) { ?>
		<div class="page-cta background-color__titles margin-top__big--x padding-top__big--x padding-bottom__big--x">
			<div class="container-fluid wrap">
				<div class="row center-xs">
					<div class="col-xs-12 col-md-8 center-xs">
						<?php if( $cta_title ) { ?>
							<h2 class="line-height__big text-color__secondary"><?php echo $cta_title; ?></h2>
						<?php } ?>
						<?php if( $cta_text ) { ?>
							<p class="font-size__big text-color__white margin-bottom__big--x"><?php echo $cta_text; ?></p>
						<?php } ?>
						<?php if( $cta_btn_text ) { ?>
							<a href="<?php echo $cta_btn_url; ?>" class="btn wow fadeInUp background-color__secondary font-size__medium btn__size--medium text-color__titles" data-wow-delay="0.4s"><?php echo $cta_btn_text; ?></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
</section>

<script>
new WOW().init();
</script>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();