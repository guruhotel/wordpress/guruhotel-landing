<?php
/* Template name: Tianguis Turístico */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="tianguis">
    <div class="background-color__titles padding-bottom__mega">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-12 col-md-8">
                    <img class="tianguis-logo" src="<?php assets_url(); ?>/images/tianguis.svg" alt="GuruHotel Tianguis Turístico" />
                    <h1 class="text-color__white">Tianguis Turístico 2020</h1>
                    <p class="font-size__big text-color__secondary"><i class="fa fa-map-marker-alt"></i> Mérida, Yucatán.</p>
                    <hr class="alpha-bg" />
                    <p class="font-size__medium text-color__white"><i class="far fa-calendar text-color__secondary margin-right__normal"></i> Del 22 al 25 de Marzo</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid wrap">
        <div class="row center-xs">
            <div class="col-xs-12 col-md-7 margin-bottom__mega">
                <h3 class="margin-top__mega">Vísita nuestro Stand #3145</h3>
                <p class="margin-top__big margin-bottom__big font-size__medium--x text-color__titles">Si planeas asistir a este magno evento, <strong>no pierdas la oportunidad de reunirte con nuestro equipo de expertos hoteleros</strong> en nuestro stand ubicado en la zona de corporativos.</p>
                <div class="card background-color__white border-color__grey--regent border-radius__normal box-shadow__medium margin-bottom__mega--x">
                    <h2>Programa tu cita</h2>
                    <p class="font-size__medium">Con alguno de nuestros expertos hoteleros.</p>
                    <div class="inbound-form-wrapper" id="form_2666" data-path="https://app.99inbound.com/i/21f0d0bf-d2cc-4575-ac76-37240cbc6695" data-token="xciWcvIHYrW6AVIKlvz4wQtt"></div>
                </div>
                <h3 class="font-weight__normal margin-bottom__mega">¡Te esperamos! 😃</h3>
                <!-- Only include this once in your webpage -->
                <script type="text/javascript" src="https://app.99inbound.com/99inbound.js"></script>
            </div>
        </div>
    </div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();