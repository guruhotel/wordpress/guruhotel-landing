<?php
/* Template name: Saving buy gift card */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="saving">
    <div class="saving-hero padding-bottom__mega">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-9">
                    <?php echo do_shortcode( '[trp_language language="en_US"]<div  data-site-id="8fa0c471-c3f0-4671-8f07-2056531bbf2f" data-platform="Other" class="gift-up-target" data-group-id="6f791b9d-5ccc-4c09-3aa0-08d7d73388a0"></div>
                    <script type="text/javascript">
                    (function (g, i, f, t, u, p, s) {
                        g[u] = g[u] || function() { (g[u].q = g[u].q || []).push(arguments) };
                        p = i.createElement(f);
                        p.async = 1;
                        p.src = t;
                        s = i.getElementsByTagName(f)[0];
                        s.parentNode.insertBefore(p, s);
                    })(window, document, "script", "https://cdn.giftup.app/dist/gift-up.js", "giftup");
                    </script>[/trp_language]' ) ?>
                    <?php echo do_shortcode( '[trp_language language="es_MX"]<div  data-site-id="8fa0c471-c3f0-4671-8f07-2056531bbf2f" data-platform="Other" class="gift-up-target" data-group-id="42e04f9a-55bb-479c-b625-08d7d7265375" data-language="es-ES"></div>
                    <script type="text/javascript">
                    (function (g, i, f, t, u, p, s) {
                        g[u] = g[u] || function() { (g[u].q = g[u].q || []).push(arguments) };
                        p = i.createElement(f);
                        p.async = 1;
                        p.src = t;
                        s = i.getElementsByTagName(f)[0];
                        s.parentNode.insertBefore(p, s);
                    })(window, document, "script", "https://cdn.giftup.app/dist/gift-up.js", "giftup");
                    </script>[/trp_language]' ) ?>
                    <img class="image" src="<?php assets_url(); ?>/images/saveindiehotels.svg" alt="Save Indie Hotels" />
                    <div class="image-bg" style="background-image: url('<?php assets_url(); ?>/images/hotels-bg.svg')"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();