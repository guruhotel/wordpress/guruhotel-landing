<?php
/* Template name: Plans */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section class="plans-template">
	<div class="container-fluid wrap">
		<div class="row center-xs between-md">
            <div class="col-xs-12 col-sm-10 col-md-5 start-xs">
                <div class="sticky">
                    <div class="logo">
                        <a href="<?php bloginfo('wpurl'); ?>">
                            <?php get_template_part('template-parts/header/logo'); ?>
                        </a>
                    </div>
                    <h1 class="white-color">Únete hoy a GuruHotel<span class="secondary-color">.</span></h1>
                    <p class="secondary-color font-size-medium">Y comienza a transformar tus ventas.</p>
                    <div class="features">
                        <div class="feature one">
                            <img
                            class="icon"
                            src="<?php assets_url(); ?>/images/user-icon.svg"
                            alt="Bell" />
                            <h2 class="font-size-normal white-color"><?php $title = theme_opt('theme_features_opt','features_one_title'); if(empty($title)) { echo 'Sin contratar inexpertos'; } else { echo $title; } ?></h2>
                            <p class="desc"><?php $desc = theme_opt('theme_features_opt','features_one_desc'); if(empty($desc)) { echo 'Nuestro equipo de expertos trabajará contigo de la mano. Sin personal adicional.'; } else { echo $desc; } ?></p>
                        </div>
                        <div class="feature two">
                            <img
                            class="icon"
                            src="<?php assets_url(); ?>/images/calendar-icon.svg"
                            alt="Bell" />
                            <h2 class="font-size-normal white-color"><?php $title = theme_opt('theme_features_opt','features_two_title'); if(empty($title)) { echo 'Aumentamos la ocupación'; } else { echo $title; } ?></h2>
                            <p class="desc"><?php $desc = theme_opt('theme_features_opt','features_two_desc'); if(empty($desc)) { echo 'Nuestra meta es llevar al máximo los niveles de ocupación de tu propiedad.'; } else { echo $desc; } ?></p>
                        </div>
                        <div class="feature three">
                            <img
                            class="icon"
                            src="<?php assets_url(); ?>/images/service-icon.svg"
                            alt="Bell" />
                            <h2 class="font-size-normal white-color"><?php $title = theme_opt('theme_features_opt','features_three_title'); if(empty($title)) { echo 'Tarifas a la medida de su negocio'; } else { echo $title; } ?></h2>
                            <p class="desc"><?php $desc = theme_opt('theme_features_opt','features_three_desc'); if(empty($desc)) { echo 'Cobramos según nuestros resultados ayudándote en la ocupación.'; } else { echo $desc; } ?></p>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xs-12 col-md-6">
				<div class="the-content start-xs card white-border white-bg">
                    <?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();