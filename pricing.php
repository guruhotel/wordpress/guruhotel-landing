<?php
/* Template name: Pricing */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="pricing">
    <div id="page-hero">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-10 col-md-8">
                    <a name="pricing"></a>
                    <h1 class><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="pricing">
        <ul data-tabs class="pricing-tabs">
            <li><a data-tabby-default href="#monthly">Mensual</a></li>
            <li><a href="#yearly">Anual</a></li>
        </ul>
        <div id="monthly">
            <?php get_template_part( 'template-parts/pricing' , 'month' ); ?>
        </div>
        <div id="yearly">
            <?php get_template_part( 'template-parts/pricing' , 'year' ); ?>
        </div>
    </div>
    <div class="faq">
        <div class="container-fluid wrap">
            <div class="center-xs margin-bottom__mega--x">
                <h2>Preguntas frecuentes</h2>
            </div>
            <div class="card margin-bottom__medium featured-faq border-radius__small--x border-color__grey--regent box-shadow__medium">
                <div class="row center-xs around-md">
                    <div class="col-xs-12 col-sm-8 col-md-5 start-xs">
                        <div class="item one">
                            <h4 class="margin-bottom__medium--x line-height__medium--x">¿A quién está dirigido GuruHotel?</h4>
                            <p>A hoteles y apartahoteles que tienen capacidad para operar sus propiedades. Nuestra solución para casas vacacionales está en camino. Súmate a la lista de espera.</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-5 start-xs">
                        <div class="item two">
                            <h4 class="margin-bottom__medium--x line-height__medium--x">¿GuruHotel es una operadora de hoteles?</h4>
                            <p>No operamos. Nos encargamos de la comercialización y distribución online, trabajamos de la mano de propietarios y operadores de hoteles comprometidos con sus huéspedes.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row center-xs around-md">
                <div class="col-xs-11 col-sm-8 col-md-5 start-xs">
                    <div class="item">
                        <h4 class="margin-bottom__medium--x line-height__medium--x">¿Quién está a cargo de mi sitio web?</h4>
                        <p>Nuestro equipo se encargará de gestionarlo en todo momento e implementar las mejoras necesarias para asegurar una mejor optimización en las reservas directas.</p>
                    </div>
                    <div class="item">
                        <h4 class="margin-bottom__medium--x line-height__medium--x">¿El sitio web incluye dominio?</h4>
                        <p>En los planes "Premium" y "Expert" te incluimos un dominio que será administrado por nosotros. Si ya tienes tu dominio solamente debes apuntarlo a nuestros servidores.</p>
                    </div>
                    <div class="item">
                        <h4 class="line-height__medium--x">¿Qué es el motor de reservas?</h4>
                        <p>Esta herramienta de comercio electrónico nos permite gestionar las reservas directas y enviarte los pagos realizados a través del sitio web.</p>
                    </div>
                </div>
                <div class="col-xs-11 col-sm-8 col-md-5 start-xs">
                    <div class="item">
                        <h4 class="margin-bottom__medium--x line-height__medium--x">¿Qué es el softbrand "By GuruHotel"?</h4>
                        <p>Es nuestro distintivo que ayudará a posicionar tu propiedad tanto en OTA's y Google My Business, como en tu sitio web hotelero.</p>
                    </div>
                    <div class="item">
                        <h4 class="margin-bottom__medium--x line-height__medium--x">¿Cómo funciona la pasarela de pagos?</h4>
                        <p>Mediante tu sitio web hotelero acepta pagos en tiempo real de tarjetas de crédito o débito, PayPal y tarjetas virtuales de Booking, Expedia, etc.</p>
                    </div>
                    <div class="item">
                        <h4 class="margin-bottom__medium--x line-height__medium--x">¿Qúe es la terminal virtual GuruPay?</h4>
                        <p>“Es como tener una terminal bancaria siempre a la mano”… Podrás realizar el cobro de tarjetas virtuales y no presenciales directamente desde el PMS.</p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row center-xs around-md padding-top__big padding-bottom__mega--x">
                <div class="col-xs-9 col-sm-6 col-md-4 start-xs margin-bottom__big">
                    <img src="<?php assets_url(); ?>/images/team.svg">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-7 start-xs">
                    <div class="experts-team">
                        <h3 class="line-height__medium--x margin-bottom__medium--x">Equipo de expertos</h3>
                        <p class="margin-bottom__big--x line-height__big--x">En GuruHotel contamos con un grupo de profesionales con años de experiencia en la industria hotelera y un equipo de expertos en tecnología que ponemos a tu disposición para optimizar al máximo tu propiedad.</p>
                        <span class="tag background-color__grey--regent border-radius__small--x padding__small padding-left__normal padding-right__normal">Revenue manager</span>
                        <span class="tag background-color__grey--regent border-radius__small--x padding__small padding-left__normal padding-right__normal">Ecommerce manager</span>
                        <span class="tag background-color__grey--regent border-radius__small--x padding__small padding-left__normal padding-right__normal">Inventory manager</span>
                        <span class="tag background-color__grey--regent border-radius__small--x padding__small padding-left__normal padding-right__normal">Content manager</span>
                        <span class="tag background-color__grey--regent border-radius__small--x padding__small padding-left__normal padding-right__normal">Sales manager</span>
                        <span class="tag background-color__grey--regent border-radius__small--x padding__small padding-left__normal padding-right__normal">Designer</span>
                        <span class="tag background-color__grey--regent border-radius__small--x padding__small padding-left__normal padding-right__normal">Web developer</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="main-cta">
    <div class="container-fluid wrap">
        <div class="card border-color__grey--regent border-radius__small--x">
            <div class="row center-xs">
                <div class="col-xs-12 col-md-7 center-xs start-md">
                    <h2 class="title text-color__white without-margin-top">Únete a GuruHotel</h2>
                    <p class="text-color__white font-size__medium without-margin">Cadena Hotelera Digital<span class="text-color__secondary">.</span></p>
                </div>
                <div class="col-xs-12 col-md-4 margin-top__medium end-md">
                    <a href="#pricing" class="btn btn__size--big font-weight__normal background-color__utilitary text-color__white">Ver planes nuevamente</a>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
var j = jQuery.noConflict();

j(document).ready(function () {
    var tabs = new Tabby('[data-tabs]');
});
</script>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();