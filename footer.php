</main>

<footer id="footer">
    <div class="container-fluid wrap">
        <div class="row">
            <div class="col-xs-12 col-md-4 center-xs start-md">
                <?php if(is_page_template('pricing.php')) { ?>
					<img src="<?php assets_url(); ?>/images/logo-original.svg" class="logo">
				<?php } else { ?>
					<img src="<?php assets_url(); ?>/images/logo-inverse.svg" class="logo">
				<?php } ?>
                <div class="social">
                    <span class="label">Síguenos</span>
                    <a href="https://www.linkedin.com/company/guruhotel" class="linkedin" target="_blank" rel="nofollow"><i class="fab fa-linkedin-in"></i></a>
                    <a href="https://www.instagram.com/guruhotelcom/" class="instagram" target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <div class="footer-menu col-xs-12 col-md-8">
                <div class="row center-xs start-md">
                    <div class="col col-xs-12 col-sm-3 col-md-4 center-xs start-sm">
                        <ul>
                            <li class="title">Enlaces rápidos</li>
                            <li><a href="<?php bloginfo('wpurl'); ?>/">Inicio</a></li>
                            <li><a href="https://app.guruhotel.com/community">Comunidad hotelera</a></li>
                            <li><a href="<?php bloginfo('wpurl'); ?>/saveindiehotels">#SaveIndieHotels</a></li>
                        </ul>
                    </div>
                    <div class="col col-xs-12 col-sm-3 col-md-4 center-xs start-sm">
                        <ul>
                            <li class="title">Productos</li>
                            <li><a href="<?php bloginfo('wpurl'); ?>/planes">Precios</a></li>
                            <li><a href="<?php bloginfo('wpurl'); ?>/gurupay">GuruPay</a></li>
                        </ul>
                    </div>
                    <div class="col col-xs-12 col-sm-3 col-md-4 center-xs start-sm">
                        <ul>
                            <li class="title">Nosotros</li>
                            <li><a href="<?php bloginfo('wpurl'); ?>/trabaja-con-nosotros">Trabaja con nosotros</a></li>
                            <li><a href="<?php bloginfo('wpurl'); ?>/afiliados">Afiliados</a></li>
                            <li><a href="<?php bloginfo('wpurl'); ?>/terminos-y-condiciones">Términos y condiciones</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 center-xs">
                <hr>
                <p>&copy; <?php echo date('Y'); ?> <strong class="titles-color">GuruHotel</strong> - Cadena Hotelera Digital</p>
            </div>
        </div>
    </div>
</footer>

<?php if(!is_page_template('saving.php') &! is_page_template('savingbuy.php')) { ?>
    <!-- Start of Async Drift Code -->
    <script>
        "use strict";
        !function() {
        var t = window.driftt = window.drift = window.driftt || [];
        if (!t.init) {
            if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
            t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
            t.factory = function(e) {
            return function() {
                var n = Array.prototype.slice.call(arguments);
                return n.unshift(e), t.push(n), t;
            };
            }, t.methods.forEach(function(e) {
            t[e] = t.factory(e);
            }), t.load = function(t) {
            var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
            o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
            var i = document.getElementsByTagName("script")[0];
            i.parentNode.insertBefore(o, i);
            };
        }
        }();
        drift.SNIPPET_VERSION = '0.3.1';
        drift.load('nyt9ky6ibzcu');
    </script>
    <!-- End of Async Drift Code -->
<?php } ?>

<?php
wp_footer(); ?>

</body>
</html>