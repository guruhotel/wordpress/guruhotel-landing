<?php
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! function_exists( 'guruhotel_theme_js' ) ) {
    function guruhotel_theme_js() {
        //Define Theme Path
        $theme_path = get_template_directory_uri();
        
        //Default JQuery
        wp_enqueue_script('jquery'); 

        wp_register_script('fontawesome-js','//kit.fontawesome.com/549f1fa78b.js',array('jquery'),false);
        wp_enqueue_script('fontawesome-js');

        wp_register_script('main-js',$theme_path.'/assets/js/main.js',array('jquery'),false);
        wp_enqueue_script('main-js');
    }
}

if ( ! function_exists( 'guruhotel_theme_css' ) ) {
    function guruhotel_theme_css() {
        //Define Theme Path
        $theme_path = get_template_directory_uri();
        
        //Deregister Default CSS
        wp_deregister_style('font-awesome.min');
        wp_deregister_style('fontawesome-css');
        wp_deregister_style('fontawsome-css');

        //Wordpress Style CSS
        wp_register_style('css', $theme_path . '/style.css', false);
        wp_enqueue_style('css');

        //Libs theme CSS
        wp_register_style('libs-css', $theme_path . '/assets/css/libs.css', false);
        wp_enqueue_style('libs-css');

        wp_register_style('main-css', $theme_path . '/assets/css/main.css', false);
        wp_enqueue_style('main-css');
    }
}

if (!function_exists('guruhotel_theme_header')) {
    function guruhotel_theme_header() {
        echo '<link rel="shortcut icon" type="image/x-icon" href="'.get_template_directory_uri().'/assets/images/favicon.png">';
    }
}

if (!function_exists('guruhotel_theme_footer')) {
    function guruhotel_theme_footer() {
        $tracking_code = theme_opt('_main_options','theme_tracking_code');
        if(!empty($tracking_code)) {
            echo $tracking_code;
        };

        echo '<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700|Poppins:400,500,700&display=swap" rel="stylesheet">';
    }
}