<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Wordpress General Enqueue
 */
add_action( 'wp_enqueue_scripts', 'guruhotel_theme_js', 10 );
add_action( 'wp_enqueue_scripts', 'guruhotel_theme_css', 10 );

/**
 * Wordpress General Head And Foot)
 */
add_action( 'wp_head', 'guruhotel_theme_header', 100 );
add_action( 'wp_footer', 'guruhotel_theme_footer', 100 );
