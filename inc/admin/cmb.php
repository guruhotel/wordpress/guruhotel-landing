<?php
add_action( 'cmb2_admin_init', 'page_template_landing_cmb' );

function page_template_landing_cmb() {
	$prefix = 'landing_';

	$cmb_landing = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'GuruHotel Landing Setup' ),
		'object_types'  => array( 'page' )
	) );

	$cmb_landing->add_field( array(
		'name'       => esc_html__( 'Description' ),
        'id'         => $prefix . 'subline',
        'default'    => 'This is an example of a description',
		'type'       => 'textarea',
    ) );
    
    $cmb_landing->add_field( array(
		'name'       => esc_html__( 'Button text' ),
        'id'         => $prefix . 'scroll_btn',
        'default'    => 'Descargar',
		'type'       => 'text_small',
	) );

	$cmb_landing->add_field( array(
		'name' => esc_html__( 'Banner image' ),
		'id'   => $prefix . 'hero_image',
		'type' => 'file'
    ) );
    
    $group_field_id = $cmb_landing->add_field( array(
		'id'          => $prefix . 'main_features',
		'type'        => 'group',
		'description' => esc_html__( 'Main features' ),
		'options'     => array(
			'group_title'   => esc_html__( 'Feature {#}' ),
			'add_button'    => esc_html__( 'Add Another Feature' ),
			'remove_button' => esc_html__( 'Remove Feature' ),
			'sortable'      => true,
			'closed'     => true
		),
    ) );

    $cmb_landing->add_group_field( $group_field_id, array(
		'name'       => esc_html__( 'Icon' ),
		'id'         => 'icon',
        'type' => 'faiconselect',
		'options_cb' => 'returnRayFapsa',
		'attributes' => array(
			'faver' => 5
		)
	) );
    
    $cmb_landing->add_group_field( $group_field_id, array(
		'name'       => esc_html__( 'Title' ),
		'id'         => 'title',
		'type'       => 'text',
    ) );
    
    $cmb_landing->add_group_field( $group_field_id, array(
		'name'       => esc_html__( 'Description' ),
		'id'         => 'desc',
		'type'       => 'textarea',
    ) );
    
    $cmb_landing->add_field( array(
		'name'    => esc_html__( 'Form content (Some desc)' ),
		'id'      => $prefix . 'form_content',
		'type'    => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 5,
		),
    ) );
    
    $cmb_landing->add_field( array(
		'name'    => esc_html__( 'Form shortcode' ),
		'id'      => $prefix . 'form_shortcode',
		'type'    => 'text_small',
	) );

	$cmb_landing->add_field( array(
		'name'    => esc_html__( 'Form subline' ),
		'id'      => $prefix . 'form_subline',
		'type'    => 'text',
    ) );

	$cmb_landing->add_field( array(
		'name'    => esc_html__( 'CTA title' ),
		'id'      => $prefix . 'cta_title',
		'type'    => 'text'
    ) );
    
    $cmb_landing->add_field( array(
		'name'    => esc_html__( 'CTA text' ),
		'id'      => $prefix . 'cta_text',
		'type'    => 'textarea'
	) );
	
	$cmb_landing->add_field( array(
		'name'    => esc_html__( 'CTA button text' ),
		'id'      => $prefix . 'cta_btn_text',
		'type'    => 'text'
	) );
	
	$cmb_landing->add_field( array(
		'name'    => esc_html__( 'CTA button URL' ),
		'id'      => $prefix . 'cta_btn_url',
		'type'    => 'text'
    ) );
}

//

add_action( 'cmb2_admin_init', 'page_template_cases_cmb' );

function page_template_cases_cmb() {
	$prefix = 'case_study_';

	$cmb_cases = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'GuruHotel Case Study' ),
		'object_types'  => array( 'page' ),
		'show_on'      => array( 'key' => 'page-template', 'value' => 'case-study.php' ),
	) );

	$cmb_cases->add_field( array(
		'name'       => esc_html__( 'Logo de la propiedad' ),
        'id'         => $prefix . 'logo',
		'type'       => 'file',
    ) );
}

//

add_action( 'cmb2_admin_init', 'cmb2_saving_posts' );

function cmb2_saving_posts() {
	$prefix = 'saving_';

	$cmb_saving = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'GuruHotel: Save indie hotels' ),
		'object_types'  => array( 'saving_hotels' )
	) );

	$cmb_saving->add_field( array(
		'name'       => esc_html__( 'Website URL / OTA' ),
        'id'         => $prefix . 'url',
		'type'       => 'text',
		'default'    => '#'
	) );

	$cmb_saving->add_field( array(
		'name'             => esc_html__( 'Price' ),
		'id'               => $prefix . 'price',
		'type'             => 'select',
		'show_option_none' => true,
		'options'          => array(
			'50' => esc_html__( '50 usd' ),
			'100'   => esc_html__( '100 usd' ),
			'150'   => esc_html__( '150 usd' )
		),
	) );
}
	