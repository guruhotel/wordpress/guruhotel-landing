<?php
function asset_path_faiconselect() {
    return get_template_directory_uri() . '/inc/admin/faiconselect'; //Change to correct path.
}
add_filter( 'sa_cmb2_field_faiconselect_asset_path', 'asset_path_faiconselect' );
require get_template_directory() . '/inc/admin/faiconselect/iconselect.php';

function theme_theme_admin() {

	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = 'yourprefix_options_display_with_tabs';
	}

	$general_options = new_cmb2_box(
		array(
			'id'           => 'main_options_page',
			'title'        => 'GuruHotel',
			'object_types' => array( 'options-page' ),
			'option_key'   => '_main_options',
			'tab_group'    => '_main_options',
			'tab_title'    => 'General',
		)
	);

	$general_options->add_field( array(
		'name' => esc_html__( 'Demo shortcode' ),
		'id'   => 'theme_demo_shortcode',
		'type' => 'text',
	) );

	$general_options->add_field( array(
		'name'    => 'Tracking code',
		'id'      => 'theme_tracking_code',
		'type'    => 'textarea_code',
	) );
    
  	// -- Hero

	$hero_options = new_cmb2_box( array(
		'id'           => 'theme_hero',
		'menu_title'   => 'Hero banner',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_hero_opt',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => 'Hero banner',
	) );
	  
	$hero_options->add_field( array(
		'name' => esc_html__( 'Hero Banner' ),
		'id'   => 'section_title_hero_banner',
		'type' => 'title',
	) );

	$hero_options->add_field( array(
		'name' => esc_html__( 'Título' ),
		'id'   => 'hero_banner_title',
		'type' => 'text',
		'deafult' => 'Cadena Hotelera Digital',
	) );

	$hero_options->add_field( array(
		'name' => esc_html__( 'Subtitulo' ),
		'id'   => 'hero_banner_subtitle',
		'type' => 'text',
		'deafult' => 'El sistema más efectivo de ocupación 100% online.',
	) );

	$hero_options->add_field( array(
		'name' => esc_html__( 'Título de CTA' ),
		'id'   => 'hero_banner_cta_title',
		'type' => 'text',
		'deafult' => 'Muchos hoteles y casas vacacionales ya confian en nosotros como su plataforma favorita.',
	) );

	$group_field_id = $hero_options->add_field( array(
		'id'          => 'hero_banner_cta_logos',
		'type'        => 'group',
		'options'     => array(
			'group_title'   => esc_html__( 'Logos del CTA #{#}' ),
			'add_button'    => esc_html__( 'Add Another' ),
			'remove_button' => esc_html__( 'Remove' ),
			'sortable'      => true,
			'closed'     => true
		),
	) );

	$hero_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Logo' ),
		'id' => 'logo',
		'type' => 'file',
	) );

	$hero_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'URL' ),
		'id' => 'url',
		'type' => 'text',
	) );

	// -- Caracetristicas

	$features_options = new_cmb2_box( array(
		'id'           => 'theme_features',
		'menu_title'   => 'Caracteristicas',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_features_opt',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => 'Caracteristicas',
	) );

	$features_options->add_field( array(
		'name' => esc_html__( 'Caracteristica #1' ),
		'id'   => 'section_features_one_title',
		'type' => 'title',
	) );

	$features_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'features_one_title',
		'type'       => 'text',
		'default' 	 => 'Sin contratar inexpertos'
	) );

	$features_options->add_field( array(
		'name'       => esc_html__( 'Descripción' ),
		'id'         => 'features_one_desc',
		'type'       => 'textarea',
		'default' 	 => 'Nuestro equipo de expertos trabajará contigo de la mano. Sin personal adicional.'
	) );

	$features_options->add_field( array(
		'name' => esc_html__( 'Caracteristica #2' ),
		'id'   => 'section_features_two_title',
		'type' => 'title',
	) );

	$features_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'features_two_title',
		'type'       => 'text',
		'default' 	 => 'Aumentamos la ocupación'
	) );

	$features_options->add_field( array(
		'name'       => esc_html__( 'Descripción' ),
		'id'         => 'features_two_desc',
		'type'       => 'textarea',
		'default' 	 => 'Nuestra meta es llevar al máximo los niveles de ocupación de tu propiedad.'
	) );

	$features_options->add_field( array(
		'name' => esc_html__( 'Caracteristica #3' ),
		'id'   => 'section_features_three_title',
		'type' => 'title',
	) );

	$features_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'features_three_title',
		'type'       => 'text',
		'default' 	 => 'Tarifas a la medida de su negocio'
	) );

	$features_options->add_field( array(
		'name'       => esc_html__( 'Descripción' ),
		'id'         => 'features_three_desc',
		'type'       => 'textarea',
		'default' 	 => 'Cobramos según nuestros resultados ayudándote en la ocupación.'
	) );

	// -- Beneficios

	$beneficts_options = new_cmb2_box( array(
		'id'           => 'theme_beneficts',
		'menu_title'   => 'Beneficios',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_beneficts_opt',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => 'Beneficios',
	) );

	$beneficts_options->add_field( array(
		'name' => esc_html__( 'Beneficio #1' ),
		'id'   => 'section_beneficts_one_title',
		'type' => 'title',
	) );

	$beneficts_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'beneficts_one_title',
		'type'       => 'text',
		'default' 	 => 'Podemos cambiar tu realidad'
	) );

	$beneficts_options->add_field( array(
		'name'       => esc_html__( 'Items' ),
		'id'         => 'beneficts_one_items',
		'type'       => 'text',
		'repeatable' 	 => true
	) );

	$beneficts_options->add_field( array(
		'name' => esc_html__( 'Beneficio #2' ),
		'id'   => 'section_beneficts_two_title',
		'type' => 'title',
	) );

	$beneficts_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'beneficts_two_title',
		'type'       => 'text',
		'default' 	 => 'Llegamos para ayudarte'
	) );

	$beneficts_options->add_field( array(
		'name'       => esc_html__( 'Items' ),
		'id'         => 'beneficts_two_items',
		'type'       => 'text',
		'repeatable' 	 => true
	) );

	$beneficts_options->add_field( array(
		'name' => esc_html__( 'Beneficio #3' ),
		'id'   => 'section_beneficts_three_title',
		'type' => 'title',
	) );

	$beneficts_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'beneficts_three_title',
		'type'       => 'text',
		'default' 	 => 'Todo un equipo trabajando para ti'
	) );

	$beneficts_options->add_field( array(
		'name'       => esc_html__( 'Items' ),
		'id'         => 'beneficts_three_items',
		'type'       => 'text',
		'repeatable' 	 => true
	) );

	// -- Quien esta detras

	$who_options = new_cmb2_box( array(
		'id'           => 'theme_who',
		'menu_title'   => '¿Quién esta detras?',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_who_opt',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => '¿Quién esta detras?',
	) );

	$who_options->add_field( array(
		'name' => esc_html__( '¿Quién esta detras?' ),
		'id'   => 'section_who_title',
		'type' => 'title',
	) );

	$who_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'who_title',
		'type'       => 'text',
		'default' 	 => '¿Quién está detrás de GuruHotel?'
	) );

	$group_field_id = $who_options->add_field( array(
		'id'          => 'team_members',
		'type'        => 'group',
		'options'     => array(
			'group_title'   => esc_html__( 'Miembro del equipo #{#}' ),
			'add_button'    => esc_html__( 'Add Another' ),
			'remove_button' => esc_html__( 'Remove' ),
			'sortable'      => true,
			'closed'     => true
		),
	) );

	$who_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Nombre' ),
		'id' => 'name',
		'type' => 'text',
	) );

	$who_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Puesto' ),
		'id' => 'position',
		'type' => 'text',
	) );

	$who_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Descripción' ),
		'id' => 'desc',
		'type' => 'textarea',
	) );

	$who_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Avatar' ),
		'id' => 'avatar',
		'type' => 'file',
	) );

	// -- Como funciona

	$how_options = new_cmb2_box( array(
		'id'           => 'theme_how',
		'menu_title'   => '¿Cómo funciona?',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_how_opt',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => '¿Cómo funciona?',
	) );

	$how_options->add_field( array(
		'name' => esc_html__( '¿Cómo funciona?' ),
		'id'   => 'section_how_title',
		'type' => 'title',
	) );

	$how_options->add_field( array(
		'name'       => esc_html__( 'Subtitulo' ),
		'id'         => 'how_subtitle',
		'type'       => 'text',
		'default' 	 => '¿Cómo funciona GuruHotel?'
	) );

	$how_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'how_title',
		'type'       => 'text',
		'default' 	 => 'Tecnología avanzada a bajo costo.'
	) );

	$how_options->add_field( array(
		'name'       => esc_html__( 'Items' ),
		'id'         => 'how_items',
		'type'       => 'text',
		'repeatable' 	 => true
	) );

	// -- Partners

	$partners_options = new_cmb2_box( array(
		'id'           => 'theme_partners',
		'menu_title'   => 'Partners',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_partners_opt',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => 'Partners',
	) );

	$partners_options->add_field( array(
		'name' => esc_html__( 'Partners' ),
		'id'   => 'section_partners_title',
		'type' => 'title',
	) );

	$partners_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'partners_title',
		'type'       => 'textarea',
		'default' 	 => 'Tenemos contratos y acceso a más de 500 agencias online y partners tecnológicos que nos permiten brindarte la mejor solución.'
	) );

	$partners_options->add_field( array(
		'name' => esc_html__( 'Logos de partners' ),
		'id'   => 'partners_logos',
		'type'         => 'file_list',
		'preview_size' => array( 100, 100 ),
	) );

	// -- CTA

	$cta_options = new_cmb2_box( array(
		'id'           => 'theme_cta',
		'menu_title'   => 'CTA',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_cta_opt',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => 'CTA',
	) );

	$cta_options->add_field( array(
		'name' => esc_html__( 'Call to Action' ),
		'id'   => 'section_cta_title',
		'type' => 'title',
	) );

	$cta_options->add_field( array(
		'name'       => esc_html__( 'Título' ),
		'id'         => 'cta_title',
		'type'       => 'text',
		'default' 	 => 'Llevamos de manera efectiva tu negocio al siguiente nivel'
	) );

	$cta_options->add_field( array(
		'name'       => esc_html__( 'Descripción' ),
		'id'         => 'cta_desc',
		'type'       => 'text',
		'default' 	 => 'Únete hoy mismo e incrementa tus ganancias.'
	) );

	// -- Case studies

	$cases_options = new_cmb2_box( array(
		'id'           => 'theme_case_studies',
		'menu_title'   => 'Case studies',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_studies',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => 'Case studies',
	) );

	$cases_options->add_field( array(
		'name' => esc_html__( 'Case studies' ),
		'id'   => 'section_case_title',
		'type' => 'title',
	) );

	$group_field_id = $cases_options->add_field( array(
		'id'          => 'case_studies',
		'type'        => 'group',
		'options'     => array(
			'group_title' => esc_html__( 'Caso #{#}' ),
			'sortable' => true,
			'closed' => true
		),
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Cita del caso' ),
		'id' => 'cite',
		'type' => 'textarea',
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Logo del caso' ),
		'id' => 'logo',
		'type' => 'file',
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Autor del caso' ),
		'id' => 'author',
		'type' => 'text',
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Imagen del caso' ),
		'id' => 'image',
		'type' => 'file',
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'URL del caso' ),
		'id' => 'url',
		'type' => 'text',
	) );

	// -- Hero case studies

	$cases_options = new_cmb2_box( array(
		'id'           => 'theme_hero_case_studies',
		'menu_title'   => 'Case studies',
		'object_types' => array( 'options-page' ),
		'option_key'   => 'theme_hero_studies',
		'parent_slug'  => '_main_options',
		'tab_group'    => '_main_options',
    	'tab_title'    => 'Case studies: Hero',
	) );

	$cases_options->add_field( array(
		'name' => esc_html__( 'Case studies' ),
		'id'   => 'section_hero_case_title',
		'type' => 'title',
	) );

	$group_field_id = $cases_options->add_field( array(
		'id'          => 'hero_case_studies',
		'type'        => 'group',
		'options'     => array(
			'group_title' => esc_html__( 'Caso #{#}' ),
			'sortable' => true,
			'closed' => true
		),
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Headline' ),
		'id' => 'headline',
		'type' => 'text',
		'sanitization_cb' => 'my_sanitization_func'
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Imagen del caso' ),
		'id' => 'image',
		'type' => 'file',
	) );

	$cases_options->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'URL del caso' ),
		'id' => 'url',
		'type' => 'text',
	) );
}
add_action( 'cmb2_admin_init', 'theme_theme_admin' );

/**
 * A CMB2 options-page display callback override which adds tab navigation among
 * CMB2 options pages which share this same display callback.
 *
 * @param CMB2_Options_Hookup $cmb_options The CMB2_Options_Hookup object.
 */
function yourprefix_options_display_with_tabs( $cmb_options ) {
	$tabs = yourprefix_options_page_tabs( $cmb_options );
	?>
	<div class="wrap cmb2-options-page option-<?php echo $cmb_options->option_key; ?>">
		<?php if ( get_admin_page_title() ) : ?>
			<h2><?php echo wp_kses_post( get_admin_page_title() ); ?></h2>
		<?php endif; ?>
		<h2 class="nav-tab-wrapper">
			<?php foreach ( $tabs as $option_key => $tab_title ) : ?>
				<a class="nav-tab<?php if ( isset( $_GET['page'] ) && $option_key === $_GET['page'] ) : ?> nav-tab-active<?php endif; ?>" href="<?php menu_page_url( $option_key ); ?>"><?php echo wp_kses_post( $tab_title ); ?></a>
			<?php endforeach; ?>
		</h2>
		<form class="cmb-form" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="POST" id="<?php echo $cmb_options->cmb->cmb_id; ?>" enctype="multipart/form-data" encoding="multipart/form-data">
			<input type="hidden" name="action" value="<?php echo esc_attr( $cmb_options->option_key ); ?>">
			<?php $cmb_options->options_page_metabox(); ?>
			<?php submit_button( esc_attr( $cmb_options->cmb->prop( 'save_button' ) ), 'primary', 'submit-cmb' ); ?>
		</form>
	</div>
	<?php
}

/**
 * Gets navigation tabs array for CMB2 options pages which share the given
 * display_cb param.
 *
 * @param CMB2_Options_Hookup $cmb_options The CMB2_Options_Hookup object.
 *
 * @return array Array of tab information.
 */
function yourprefix_options_page_tabs( $cmb_options ) {
	$tab_group = $cmb_options->cmb->prop( 'tab_group' );
	$tabs      = array();

	foreach ( CMB2_Boxes::get_all() as $cmb_id => $cmb ) {
		if ( $tab_group === $cmb->prop( 'tab_group' ) ) {
			$tabs[ $cmb->options_page_keys()[0] ] = $cmb->prop( 'tab_title' )
				? $cmb->prop( 'tab_title' )
				: $cmb->prop( 'title' );
		}
	}

	return $tabs;
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function theme_opt( $opt_key = '', $key = '', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		// Use cmb2_get_option as it passes through some key filters.
		return cmb2_get_option( $opt_key, $key, $default );
	}
	// Fallback to get_option if CMB2 is not loaded yet.
	$opts = get_option( '_main_options', $default );
	$val = $default;
	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}
	return $val;
}

function my_sanitization_func( $original_value, $args, $cmb2_field ) {
	return $original_value; // Unsanitized value.
}