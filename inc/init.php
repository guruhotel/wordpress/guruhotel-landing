<?php
//Theme functions
require_once('theme-functions/functions.php');
require_once('theme-functions/hooks.php');

//Theme metaboxes
require_once('admin/init.php');
require_once('admin/admin.php');
require_once('admin/cmb.php');

// Theme setup
function wp_theme_setup() {
    //Add post thumbnails support
    add_theme_support('post-thumbnails');
    
    //Add custom post thumbnails size
    add_image_size( 'post-thumb', 500, 430, false );

    //Add menus support
    add_theme_support('menus');
    
    //Add custom menus
    register_nav_menus(array(
        'footer' => __('Footer menu'),
    ));
}
add_action('after_setup_theme', 'wp_theme_setup');

//Get template directory url
function assets_url() {
    echo get_template_directory_uri().'/assets';
}
?>