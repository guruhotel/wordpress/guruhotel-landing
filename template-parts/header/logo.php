<svg xmlns="http://www.w3.org/2000/svg" width="192.563" height="60.79" viewBox="0 0 192.563 60.79">
  <g id="Grupo_6" data-name="Grupo 6" transform="translate(-171.245 -226)">
    <g id="Grupo_3" data-name="Grupo 3" transform="translate(171.245 241.497)">
      <g id="Logotipo_Guruhotel_Color_Bg" data-name="Logotipo Guruhotel Color Bg" transform="translate(0)">
        <path id="Trazado_1" data-name="Trazado 1" d="M113.764,111.872v14.457c0,6.557-3.547,9.609-9.671,9.609a12.807,12.807,0,0,1-5.521-1.17V129.2a11.223,11.223,0,0,0,5.521,1.64c2.471,0,4.217-1.338,4.217-3.747v-.442a6.64,6.64,0,0,1-4.82,1.941c-4.583,0-7.764-3.647-7.764-8.5s3.181-8.567,7.731-8.567a6.351,6.351,0,0,1,4.851,2.041v-1.7Zm-9,4.987a3.212,3.212,0,1,0,3.212,3.212A3.212,3.212,0,0,0,104.768,116.859Z" transform="translate(-95.727 -105.648)" fill="#fff" class="text" />
        <path id="Trazado_2" data-name="Trazado 2" d="M220.188,128.411a4.68,4.68,0,0,1-4.118,2.21,5.712,5.712,0,0,1-5.723-6.157V113.385H215.8v9.432c0,1.539.836,2.471,2.176,2.471a2.155,2.155,0,0,0,2.21-2.242V113.38h5.452v16.866h-5.452Z" transform="translate(-189.234 -107.162)" fill="#fff" class="text" />
        <path id="Trazado_3" data-name="Trazado 3" d="M315.861,113.505a4.541,4.541,0,0,1,3.815-1.941,8.56,8.56,0,0,1,1.272.1v5.756a6.667,6.667,0,0,0-2.678-.535,2.3,2.3,0,0,0-2.409,2.242v9.638H310.4V111.9h5.452Z" transform="translate(-270.854 -105.675)" fill="#fff" class="text" />
        <path id="Trazado_4" data-name="Trazado 4" d="M390.938,128.411a4.68,4.68,0,0,1-4.118,2.21,5.71,5.71,0,0,1-5.721-6.157V113.385h5.452v9.432c0,1.539.836,2.471,2.176,2.471a2.155,2.155,0,0,0,2.21-2.242V113.38h5.452v16.866h-5.452Z" transform="translate(-328.532 -107.162)" fill="#fff" class="text" />
        <path id="Trazado_5" data-name="Trazado 5" d="M486.642,87.7a4.855,4.855,0,0,1,4.118-2.21,5.717,5.717,0,0,1,5.756,6.157v11.043h-5.452V93.257a2.17,2.17,0,0,0-2.174-2.442,2.125,2.125,0,0,0-2.243,2.242v9.638h-5.452V79.6h5.452Z" transform="translate(-410.182 -79.6)" fill="#fff" class="text" />
        <path id="Trazado_6" data-name="Trazado 6" d="M584.04,111.568a8.785,8.785,0,1,1-8.768,8.774,8.63,8.63,0,0,1,8.768-8.774Zm0,12.249a3.47,3.47,0,1,0-3.242-3.481,3.323,3.323,0,0,0,3.242,3.481Z" transform="translate(-486.933 -105.679)" fill="#fff" class="text" />
        <path id="Trazado_7" data-name="Trazado 7" d="M677.636,110.1V97.878H675.36V93.236h2.608V89.79l5.12-1.105v4.551H685.7v4.652h-2.608V110.1Z" transform="translate(-568.582 -87.005)" fill="#fff" class="text" />
        <path id="Trazado_8" data-name="Trazado 8" d="M740.985,121.979c.4,1.44,1.808,2.276,3.782,2.276a11.549,11.549,0,0,0,5.555-1.338v5.12a14.461,14.461,0,0,1-6.025,1.1,8.787,8.787,0,1,1-.335-17.571,7.535,7.535,0,0,1,7.764,7.9,11.017,11.017,0,0,1-.3,2.505Zm-.033-4.048h5.588c-.1-1.105-1.1-1.875-2.608-1.875a2.965,2.965,0,0,0-2.979,1.874Z" transform="translate(-617.678 -105.682)" fill="#fff" class="text" />
        <path id="Trazado_9" data-name="Trazado 9" d="M840.124,102.69h-5.452V79.6h5.452Z" transform="translate(-698.547 -79.6)" fill="#fff" class="text" />
      </g>
    </g>
    <g id="Grupo_5" data-name="Grupo 5" transform="translate(326.111 226)">
      <g id="Entrada" transform="translate(0 3.894)">
        <path id="Entrada_Portal" data-name="Entrada Portal" d="M133.381,100.792,127.1,149.457h18.849V100.792Z" transform="translate(-127.1 -100.792)" fill="#d7f1f8"/>
        <path id="Entrance_Light" data-name="Entrance Light" d="M127.1,149.457h6.283V100.792Z" transform="translate(-127.1 -100.792)" fill="#6fc9e4" opacity="0.5"/>
      </g>
      <path id="Door_Bottom" data-name="Door Bottom" d="M223.642,80.844v3.894h18.849Z" transform="translate(-204.793 -80.844)" fill="#e16941"/>
      <path id="Door_Middle" data-name="Door Middle" d="M223.642,100.792v48.667h18.849V100.792Z" transform="translate(-204.793 -96.898)" fill="#edb256"/>
      <path id="Door_Nob" data-name="Door Nob" d="M245.557,234.059l-1.34.255v-8.346l1.34-.137Z" transform="translate(-221.35 -197.528)" fill="#1f205e"/>
      <path id="Door_Top" data-name="Door Top" d="M223.642,358.29l18.849-8.236H223.642Z" transform="translate(-204.793 -297.5)" fill="#e16941"/>
    </g>
  </g>
</svg>
