<div class="container-fluid wrap">
    <div class="row center-xs">
        <div class="col-xs-12 col-md-4 start-xs">
            <div class="item card border-color__grey--regent box-shadow__medium card__size--big margin-bottom__big">
                <div class="header">
                    <h3 class="text-color__utilitary">Starter</h3>
                    <div class="price">
                        <span class="font-size__normal text-color__titles">$</span>
                        <strong class="font-size__big text-color__titles">0</strong>
                        <span class="font-size__normal text-color__titles">USD / mes</span>
                    </div>
                    <div>
                        <p class="font-size__small--x">5% de comisión por reserva</p>
                    </div>
                </div>
                <ul class="features">
                    <li><i class="far fa-check-square"></i> <span class="label">Sitio web hotelero + Hosting + SSL</span></li>
                    <li><i class="far fa-check-square"></i> <span class="label">Pasarela de pagos online</span></li>
                    <li><i class="far fa-check-square"></i> <span class="label">Motor de reservas directas</span></li>
                    <li><i class="far fa-check-square"></i> <span class="label">Softbrand "By GuruHotel"</span></li>
                    <li><i class="far fa-check-square"></i> <span class="label">Optimización Google My Business</span></li>
                </ul>
                <a href="https://app.guruhotel.com/register?ref=basic-plan-year" class="btn margin-top__medium btn__size--medium btn__size--full background-color__utilitary text-color__white">Continuar gratis</a>
            </div>
        </div>
        <div class="col-xs-12 col-md-4 start-xs">
            <div class="item two card border-color__utilitary box-shadow__medium card__size--big margin-bottom__big">
                <div class="header">
                    <span class="padding__small--x padding-left__medium padding-right__medium font-size__small--x border-radius__small--x background-color__utilitary text-color__white">Mejor valorado</span>
                    <h3 class="text-color__utilitary">Premium</h3>
                    <div class="price">
                        <span class="font-size__normal text-color__titles">$</span>
                        <strong class="font-size__big text-color__titles">259</strong>
                        <span class="font-size__normal text-color__titles">USD / mes</span>
                        <span class="badge">25% descuento</span>
                    </div>
                    <div>
                        <p class="font-size__small--x">5% de comisión por reserva</p>
                    </div>
                </div>
                <ul class="features">
                    <li><i class="far fa-check-square"></i> <span class="label">Incluye características <strong>"Basic"</strong></span></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Property management system</span></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Channel manager</span></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Comparador de precios</span></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Fotografías profesionales</span></li>
                </ul>
                <a href="https://app.guruhotel.com/register?ref=premium-plan-year" class="btn margin-top__medium btn__size--medium btn__size--full background-color__utilitary text-color__white">Seleccionar premium</a>
            </div>
        </div>
        <div class="col-xs-12 col-md-4 start-xs">
            <div class="item card border-color__grey--regent box-shadow__medium card__size--big margin-bottom__big">
                <div class="header">
                    <span class="padding__small--x padding-left__medium padding-right__medium font-size__small--x border-radius__small--x background-color__grey text-color__text">Recomendado</span>
                    <h3 class="text-color__utilitary">Expert</h3>
                    <div class="price">
                        <span class="font-size__normal text-color__titles">$</span>
                        <strong class="font-size__big text-color__titles">379</strong>
                        <span class="font-size__normal text-color__titles">USD / mes</span>
                        <span class="badge">25% descuento</span>
                    </div>
                    <div>
                        <p class="font-size__small--x">5% de comisión por reserva</p>
                    </div>
                </div>
                <ul class="features">
                    <li><i class="far fa-check-square"></i> <span class="label">Incluye características <strong>"Premium"</strong></span></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Equipo de expertos</span></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Optimización y gestión de OTA's</span> <strong class="text-color__orange display__block font-size__small--x">*Libre de comisiones</strong></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Apertura de nuevos canales</span></li>
                    <li><i class="far fa-plus-square"></i> <span class="label">Terminal virtual GuruPay</span></li>
                </ul>
                <a href="https://app.guruhotel.com/register?ref=expert-plan-year" class="btn margin-top__medium btn__size--medium btn__size--full background-color__utilitary text-color__white">Seleccionar expert</a>
            </div>
        </div>
        <div class="col-xs-12 start-xs">
            <div class="item enterprise card background-color__grey card__size--big margin-bottom__big">
                <div class="row middle-xs around-xs">
                    <div class="col-xs-12 col-md-2 center-xs start-md">
                        <div class="header">
                            <h3 class="text-color__utilitary without-margin">Enterprise</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 center-xs">
                        <p class="text-color__titles font-size__medium margin-top__medium margin-bottom__medium"><strong>¿Tienes más de 100 habitaciones?</strong> GuruHotel a la medida de tu hotel con enterprise.</p>
                    </div>
                    <div class="col-xs-12 col-md-3  center-xs end-md">
                        <a href="https://guruhotel.com/plan-enterprise/" class="btn background-color__grey--regent font-weight__bold btn__size--medium btn__size--full text-color__text">Preguntar por enterprise</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>