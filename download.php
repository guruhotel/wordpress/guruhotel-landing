<?php
/* Template name: Download */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="pricing">
    <div id="page-hero">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-12 col-md-8">
                    <h1 class="line-height__medium">Descargar</h1>
                    <p class="font-size__medium--x">GuruHotel PMS: Solución de escritorio</p>
                </div>
            </div>
        </div>
    </div>
    <div class="downloads">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-12 col-sm-8 col-md-5 start-xs">
                    <a href="https://dl.todesktop.com/200130n3clf3vjg/mac" class="btn item border-radius__small--x box-shadow__medium font-size__medium--x margin-top__medium btn__size--big btn__size--full background-color__white border-color__grey--regent text-color__titles">
                        <i class="fab fa-apple icon margin-right__normal font-size__big--x"></i>
                        <span class="label">Descargar para Mac</span>
                    </a>
                    <a href="https://dl.todesktop.com/200130n3clf3vjg/windows" class="btn item border-radius__small--x box-shadow__medium font-size__medium--x margin-top__medium btn__size--big btn__size--full background-color__white border-color__grey--regent text-color__titles">
                        <i class="fab fa-windows icon margin-right__normal font-size__big--x"></i>
                        <span class="label">Descargar para Windows</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-8 margin-top__big--x start-xs">
                    <img src="<?php assets_url(); ?>/images/pms.png">
                </div>
            </div>
        </div>
    </div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();