<?php
/* Template name: Landings Page Template */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="page-template-landing">
	<div class="titles-bg hero-page">
		<div class="container-fluid wrap">
			<div class="row middle-xs center-xs between-sm">
				<div class="col-xs-12 col-sm-6 col-md-5 center-xs start-sm">
					<div class="hero-caption center-xs start-sm">
						<h1 class="font-size-x-big wow fadeInUp center-xs start-sm white-color without-margin-top"><?php the_title(); ?></h1>
						<?php
						$subline = get_post_meta(get_the_ID(), 'landing_subline', true);
						if( !empty($subline) ) { ?>
							<p class="font-size-medium wow fadeInUp center-xs start-sm margin-top-medium margin-bottom-medium" data-wow-delay="0.2s"><?php echo $subline; ?></p>
						<?php } ?>
						<?php
						$scroll_btn = get_post_meta(get_the_ID(), 'landing_scroll_btn', true);
						if( !empty($scroll_btn) ) { ?>
							<a href="#contact-form" class="btn wow fadeInUp secondary-bg secondary-border font-size-normal big titles-color" data-wow-delay="0.4s"><?php echo $scroll_btn; ?></a>
						<?php } ?>
					</div>
				</div>
				<?php
				$hero_image = get_post_meta(get_the_ID(), 'landing_hero_image', true);
				if( !empty($hero_image) ) { ?>
					<div class="col-xs-12 hero-img col-sm-6 position-relative">
						<img src="<?php echo $hero_image; ?>" class="wow fadeIn"  data-wow-delay="0.6s">
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php
	$main_features = get_post_meta(get_the_ID(), 'landing_main_features', true);
	if( !empty($main_features) ) { ?>
		<div class="container-fluid wrap template-landing-main-features margin-top-big margin-bottom-x-big">
			<div class="row">
				<?php
				foreach ( $main_features as $feature ) { ?>
					<div class="feature wow fadeInUp col-xs-12 col-sm-6 col-md-4">
						<div class="card grey-bg grey-border">
							<i class="<?php echo $feature['icon']; ?> icon font-size-x-medium main-color"></i>
							<h3 class="title font-size-medium margin-top-normal">
								<?php echo $feature['title']; ?>
							</h3>
							<p><?php echo $feature['desc']; ?></p>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
	<div id="contact-form" class="white-bg padding-top-x-big padding-bottom-x-big">
		<div class="container-fluid wrap">
			<div class="row center-xs between-md">
				<div class="the-content start-xs wow fadeInUp col-xs-12 col-md-5">
					<?php
					$form_content = get_post_meta(get_the_ID(), 'landing_form_content', true);
					if( !empty($form_content) ) {
						echo wpautop($form_content);
					} ?>
				</div>
				<div class="col-xs-12 col-md-6 start-xs">
					<div id="page-form" class="card wow fadeInUp rounded-small page-form titles-bg white-color titles-border" data-wow-delay="0.3s">
						<?php
						$form_shortcode = get_post_meta(get_the_ID(), 'landing_form_shortcode', true);
						if( $form_shortcode ) {
							echo do_shortcode($form_shortcode);
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid wrap">
		<div class="row center-xs">
			<div class="col-xs-12 col-md-5 start-xs">
				<div class="the-content"><?php the_content(); ?></div>
			</div>
		</div>
	</div>
</section>

<script>
new WOW().init();
</script>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();