<?php
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section>
	<div class="titles-bg">
		<div class="alpha-bg-inverse padding-top-small padding-bottom-small margin-bottom-big">
			<div class="container-fluid wrap">
				<div class="row center-xs align-center">
					<div class="col-xs-12 col-md-9">
						<h1 class="font-size-medium white-color"><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid wrap">
		<div class="row center-xs">
			<div class="col-xs-12 col-md-9 start-xs">
				<div class="the-content card white-bg regent-grey-border titles-color big box-shadow">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php 
get_footer();