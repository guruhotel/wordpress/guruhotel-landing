<?php
require_once('inc/init.php');

/**
 * Include theme updater
 */
require 'plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/guruhotel/wordpress/guruhotel-landing',
	__FILE__,
	'guruhotel-landing'
);

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

// Saving hotels
require_once('inc/saving.php');


?>