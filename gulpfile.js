const { src, dest, watch, series, parallel } = require('gulp');
const del = require('del');
const gulpLoadPlugins = require('gulp-load-plugins');
const server = require('browser-sync');
const pjson = require('./package.json');

const $ = gulpLoadPlugins();
const sourceDir = 'src';
const finalDir = 'assets';

const onError = err => console.log(err);

async function clean() {
  return await del.sync([finalDir, `${pjson.name}.zip`]);
}

function mainSass() {
  const autoprefixer = require('autoprefixer');

  return src([
    `${sourceDir}/sass/main.scss`,
    
  ])
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.sourcemaps.init())
    .pipe($.sass({ includePaths: ['node_modules'] }))
    .pipe($.postcss([autoprefixer()]))
    .pipe($.sourcemaps.write('.'))
    .pipe(dest(`${finalDir}/css`))
    .pipe(server.stream());
}

function libCSS() {  
    return src([
        `${sourceDir}/lib/slick-carousel/slick/slick.css`,
        `${sourceDir}/lib/tabbyjs/dist/css/tabby-ui.css`,
        `${sourceDir}/lib/animate.css/animate.min.css`
      ])
      .pipe($.plumber({ errorHandler: onError }))
      .pipe($.concatCss('libs.css'))
      .pipe(dest(`${finalDir}/css`))
      .pipe(server.stream());
}

function mainJS() {
  return src([
      `${sourceDir}/lib/wow/dist/wow.js`,
      `${sourceDir}/lib/tabbyjs/dist/js/tabby.polyfills.js`,
      `${sourceDir}/lib/slick-carousel/slick/slick.js`,
      `${sourceDir}/js/main.js`
    ])
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.concat('main.js'))
    .pipe(dest(`${finalDir}/js/`))
    .pipe(server.stream());
}

function images() {
  return src(`${sourceDir}/images/**/*`)
    .pipe(
      $.cache(
        $.imagemin({
          interlaced: true,
          progressive: true,
          optimizationLevel: 4,
          svgoPlugins: [{ removeViewBox: true }, { cleanupIDs: false }]
        })
      )
    )
    .pipe(dest(`${finalDir}/images`));
}

function watchTask() {
  server({
    notify: false,
    port: 5000,
    proxy: pjson.proxy,
    injectChanges: true,
    files: [
      '**/*.php',
      `${sourceDir}/images/**/*`,
    ]
  });

  watch(`${sourceDir}/sass/**/*.scss`, mainSass);
  watch(`${sourceDir}/js/**/*.js`, mainJS);
}

async function build() {
  const cssnano = require('cssnano');

  await del.sync(['**/*.map']);

  return src(`${finalDir}/**/*`)
    .pipe($.if('*.js', $.uglify()))
    .pipe(
      $.if(
        '*.css',
        $.postcss([
          cssnano({
            discardComments: {
              removeAll: true
            },
            discardDuplicates: true,
            discardEmpty: true,
            minifyFontValues: true,
            minifySelectors: true
          })
        ])
      )
    )
    .pipe(dest(`${finalDir}/`));
}

function zip() {
  return src([
    '**/*',

    // include specific files and folders
    'screenshot.png',

    // exclude files and folders
    '!src',
    '!src/**/*',
    '!node_modules',
    '!node_modules/**/*',
    '!cache',
    '!cache/**/*',
    '!**/*.map',
    '!**/*.md',
    '!**/*.txt',
    '!gulpfile.js',
    '!webpack.config.js',
    '!package.json',
    '!package-lock.json',
    '!yarn.lock',
    '!composer.json',
    '!composer.lock'
  ])
    .pipe($.zip(`${pjson.name}.zip`))
    .pipe(dest('.'));
}

exports.watch = series(clean, parallel( mainSass, libCSS, mainJS,  images), watchTask);
exports.build = series(
  clean,
  parallel( mainSass, libCSS, mainJS,  images),
  build
);
exports.zip = series(
  clean,
  parallel( mainSass, libCSS, mainJS,  images),
  build,
  zip
);
