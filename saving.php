<?php
/* Template name: Saving */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="saving">
    <div class="saving-hero padding-bottom__mega">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-9">
                    <h1 class="hero-title font-weight__normal margin-bottom__big"><div class="wow fadeInUp" data-wow-duration="1s">Salvemos a los</div> <strong data-wow-duration="1s" class="wow fadeInUp display__block" data-wow-delay="0.2s" style="color: #4D4CAC;">hoteles independientes</strong></h1>
                    <p class="hero-desc margin-bottom__mega">La mayoría de los hoteles independientes han tenido que suspender sus actividades y miles de personas están por perder su trabajo. <strong>¡Es momento de ayudarlos!</strong></p>
                    <?php echo do_shortcode( '[trp_language language="en_US"]<a href="https://guruhotel.com/en/saveindiehotels/buy" class="btn wow heartBeat hero-btn btn__size--medium font-weight__normal background-color__pink rounded text-color__white" data-wow-duration="1s" data-wow-delay="2s">Comprar gift card <i class="far fa-heart margin-left__small alpha-color"></i></a>[/trp_language]' ) ?>
                    <?php echo do_shortcode( '[trp_language language="es_MX"]<a href="https://guruhotel.com/saveindiehotels/buy" class="btn wow heartBeat hero-btn btn__size--medium font-weight__normal background-color__pink rounded text-color__white" data-wow-duration="1s" data-wow-delay="2s">Comprar gift card <i class="far fa-heart margin-left__small alpha-color"></i></a>[/trp_language]' ) ?>
                    <img class="image wow fadeIn" data-wow-duration="2s" data-wow-delay="0.6s" src="<?php assets_url(); ?>/images/saveindiehotels.svg" alt="Save Indie Hotels" />
                    <div class="image-bg" style="background-image: url('<?php assets_url(); ?>/images/hotels-bg.svg')"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid wrap">
        <div class="saving-steps row center-xs">
            <div class="col-xs-10 start-xs col-sm-9 col-md-4">
                <div class="item wow fadeIn" data-wow-delay="0.2s">
                    <span class="saving-steps__number">1</span>
                    <h4 class="saving-steps__title">Ayuda a un hotel</h4>
                    <p class="saving-steps__desc">Identifica el monto por el valor del gift card del hotel que quieres ayudar.</p>
                </div>
            </div>
            <div class="col-xs-10 start-xs col-sm-9 col-md-4">
                <div class="item wow fadeIn" data-wow-delay="0.4s">
                    <span class="saving-steps__number">2</span>
                    <h4 class="saving-steps__title">Compra tu gift card</h4>
                    <p class="saving-steps__desc">Haz clic en "Comprar gift card", págalo online y recibelo en tu correo electrónico.</p>
                </div>
            </div>
            <div class="col-xs-10 start-xs col-sm-9 col-md-4">
                <div class="item wow fadeIn" data-wow-delay="0.6s">
                    <span class="saving-steps__number">3</span>
                    <h4 class="saving-steps__title">Redímelo</h4>
                    <p class="saving-steps__desc">Redime tu gift card en un hotel ingresando a <a href="https://app.guruhotel.com/redeem" target="_blank" rel="nofollow">https://app.guruhotel.com/redeem</a></p>
                </div>
            </div>
        </div>
        <div class="saving-problem">
            <div class="row center-xs">
                <div class="col-xs-12">
                    <h2 class="margin-bottom__mega">¿Cuál es el problema?</h2>
                </div>
                <div class="col-xs-11 start-xs font-size__medium col-md-9 text-color__titles margin-bottom__mega">
                    <p>La aparición del COVID-19 y su rápida expansión a nivel mundial ha traído consigo estragos para la economía de todos los sectores, siendo uno de los principales afectados: el turismo.</p>
                    <p>En destinos mexicanos como Chiapas y la Riviera Maya el daño ha sido innegable, tanto que en éste último se han suspendido actividades en 500 establecimientos.  Es por esto que <strong>GuruHotel lanza Save Indie Hotels</strong>, una iniciativa para crear una red de apoyo en favor de los hoteles independientes. Desde nuestra plataforma podrás adquirir unas gift cards y canjearlas en cualquier hotel que se haya sumado a la iniciativa, una vez que las circunstancias nos permitan viajar nuevamente.</p>
                    <p><strong>Save Indie Hotels</strong> es nuestra oportunidad para ayudar a un sector en el que miles de empleos están a punto de perderse. Si a ti también te importa el bienestar de empresas y familias, ayúdanos a que se mantengan a flote y puedan seguir con la esperanza de operar cuando todo esto termine.</p>
                </div>
            </div>
        </div>
        <div class="saving-covid row center-xs wow fadeIn">
            <div class="col-xs-12 col-md-9">
                <div class="item row middle-xs center-xs around-md">
                    <div class="saving-covid__image col-xs-12 col-sm-2">
                        <img src="<?php assets_url(); ?>/images/mask-user.png" />
                    </div>
                    <div class="saving-covid__text col-xs-11 col-sm-9 start-xs">
                        <p>Además por cada gift card que compres o regales, los hoteles donarán el 10%  para comprar máscaras protectoras a médicos y enfermeras que se están enfrentando a la emergencia del COVID-19.</p>
                        <a href="https://combatingcovid.com/products/face-mask" target="_blank" class="highlight utilitary-border font-weight__normal" rel="nofollow">Ver mascaras que serán donadas <i class="fas fa-arrow-right font-size__small"></i></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 margin-top__mega">
            <h2 class="margin-bottom__medium center-xs">Hoteles independientes</h2>
            <div class="saving-hotels">
                <div class="selector">
                    <div class="center-xs text-color__titles margin-bottom__small text-transform__uppercase font-size__small letter-spacing__medium"><i class="fas fa-map-marker-alt"></i> Filtrar por ubicación:</div>
                    <ul data-tabs class="saving-hotels__tabs center-xs">
                        <?php
                        $terms = get_terms([
                            'taxonomy' => 'saving-tax',
                            'hide_empty' => true,
                            'orderby'    => 'name',
                            'order'      => 'ASC',
                        ]);
                        if( $terms ) :
                            $i = 0;
                            foreach ( $terms as $item ) { ; $i++; ?>
                                <li class="option" values="<?php echo $i ?>"><a <?php if($i==1) { echo 'data-tabby-default'; }; ?> href="#tab-<?php echo $item->slug; ?>"><?php echo $item->name; ?> <i class="fas fa-chevron-down down-icon"></i></a></li>
                            <?php }
                        endif; ?>
                    </ul>
                </div>
                <?php
                $terms = get_terms([
                    'taxonomy' => 'saving-tax',
                    'hide_empty' => true,
                    'orderby'    => 'name',
                    'order'      => 'ASC',
                ]);
                if( $terms ) :
                    $i = 0;
                    foreach ( $terms as $item ) { $i++; ?>
                        <?php
                        $wp_query = new WP_Query(array(
                            'post_type' => 'saving_hotels',
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'saving-tax',
                                    'field' => 'slug',
                                    'terms' => $item->slug,
                                )
                            ),
                        )); ?>
                        <div id="tab-<?php echo $item->slug; ?>">
                            <div class="saving-hotels-row">
                                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                    <div class="saving-hotel">
                                        <a href="<?php echo get_post_meta(get_the_ID(),'saving_url',true) ?>" target="_blank" rel="nofollow">
                                            <div class="card card__size--big border-color__grey--regent box-shado__mega border-radius__normal">
                                                <div class="saving-hotel__thumb" style="background-image: url('<?php the_post_thumbnail_url('post-thumb'); ?>');">
                                                    <img src="<?php the_post_thumbnail_url('post-thumb'); ?>" />
                                                </div>
                                                <div class="saving-hotel__caption">
                                                    <h2 class="saving-hotel__caption--title"><?php the_title(); ?></h2>
                                                    <?php
                                                        $get_price = get_post_meta(get_the_ID(),'saving_price',true);
                                                        if($get_price == '150') {
                                                            echo '<span class="text-color__pink display__block">$150 gift card</span>';
                                                        } else if($get_price == '100') {
                                                            echo '<span class="text-color__pink display__block">$100 gift card</span>';
                                                        } else {
                                                            echo '<span class="text-color__pink display__block">$50 gift card</span>';
                                                        }
                                                    ?>
                                                    <span class="font-size__small--x">Por 2 noches / 2 personas</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php endwhile; wp_reset_query(); ?>
                            </div>
                        </div>
                    <?php }
                endif; ?>
            </div>
        </div>
    </div>
    <div class="saving-cta">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-10 col-sm-9">
                    <h3 class="saving-cta__title text-color__white line-height__big">¿Listo para ayudar? Compra tu gift card y ayuda a cientos de hoteles independientes y personal médico a combatir al Coronavirus.</h3>
                    <?php echo do_shortcode( '[trp_language language="en_US"]<a href="https://guruhotel.com/en/saveindiehotels/buy" class="btn saving-cta__btn wow fadeIn">Comprar gift card <i class="far fa-heart"></i></a>[/trp_language]' ) ?>
                    <?php echo do_shortcode( '[trp_language language="es_MX"]<a href="https://guruhotel.com/saveindiehotels/buy" class="btn saving-cta__btn wow fadeIn">Comprar gift card <i class="far fa-heart"></i></a>[/trp_language]' ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="saving-cta__hotels background-color__titles padding-top__mega padding-bottom__mega">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs around-sm">
                <div class="col-xs-11 col-sm-6 center-xs start-sm">
                    <p><span class="text-color__white">¿Tienes un hotel?</span> Únete a nuestra campaña.</p>
                </div>
                <div class="col-xs-12 col-sm-3 center-xs end-sm">
                    <a href="https://app.guruhotel.com/register?ref=saveindiehotels" class="btn saving-cta__btn">Registrar mi hotel</a>
                </div>
            </div>
        </div>
    </div>
    <div class="saving-shares">
        <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog','width=626,height=436');return false;" class="facebook"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="https://twitter.com/intent/tweet?status=<?php the_permalink(); ?>" class="twitter"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
    </div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<script>
var j = jQuery.noConflict();

j(document).ready(function () {
    var tabs = new Tabby('[data-tabs]');
    
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    // Filter by query URL
    var query = getUrlParameter('filter');
    if(query) {
        tabs.toggle(`#tab-${query}`);
    }
});

j('.selector ul li.option').click(function() {
    var a = j(this).siblings().toggle();
    j(this).parent().prepend(this);
})


new WOW().init();
</script>

<?php 
get_footer();